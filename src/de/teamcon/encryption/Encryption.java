package de.teamcon.encryption;

import java.io.ByteArrayOutputStream;
import java.security.*;
import sun.misc.*;

public class Encryption {
	//Attributes
	private String algorithm;
    
	/** Creates new Encryption with a given algorithm */
	public Encryption(String algorithm) {
		this.algorithm = algorithm;	
	}

	/** Encrypts a String with the choosen Encryption and
	 * 	returns the encryted raw bytes
	 */    
	public byte[] encryptRaw(String text) throws EncException {
		try {
			MessageDigest digest = null;
			digest = MessageDigest.getInstance(algorithm);
			digest.reset();
			byte[] bytearray = text.getBytes();
			digest.update(bytearray);
			return digest.digest();
		}
		catch (Exception e) {
			throw new EncException(e.getClass().getName() + ": " + e.getMessage());
		}
	}
    
	/** Encrypts a String with the choosen Encryption and
	 * 	returns a String encoded with Base64
	 */ 
	public String encryptBase64(String text) throws EncException {
		//Encrypt
		byte[] encryptedString = this.encryptRaw(text);
		//Encode and return
		BASE64Encoder encoder = new BASE64Encoder();
		return encoder.encode(encryptedString);
	}
    
	/** Encrypts a String with the choosen Encryption and
	 * 	returns a String of hexadecimal digits
	 */  
	public String encryptHex(String text) throws Exception {
		//Encrypt
		byte[] encryptedString = this.encryptRaw(text);
		//Encode and return
		return this.convert(encryptedString);
	}
    
	/** Encrypts a String with the choosen Encryption and
	 * 	returns a String of hexadecimal digits
	 * 	@deprecated Incorrect implementation, use encryptHex instead
	 */  
	public String encryptHexOld(String text) throws Exception {
		//Encrypt
		byte[] encryptedString = new String(this.encryptRaw(text)).getBytes();
		//Encode and return
		return this.convert(encryptedString);
	}
    
	/**
	 * Convert a String of hexadecimal digits into the corresponding
	 * byte array by encoding each two hexadecimal digits as a byte.
	 */
	private byte[] convert(String digits) {
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		for (int i = 0; i < digits.length(); i += 2) {
			char c1 = digits.charAt(i);
			if ((i+1) >= digits.length())
				throw new IllegalArgumentException("Odd number of hexadecimal digits");
			char c2 = digits.charAt(i + 1);
			byte b = 0;
			if ((c1 >= '0') && (c1 <= '9'))
				b += ((c1 - '0') * 16);
			else if ((c1 >= 'a') && (c1 <= 'f'))
				b += ((c1 - 'a' + 10) * 16);
			else if ((c1 >= 'A') && (c1 <= 'F'))
				b += ((c1 - 'A' + 10) * 16);
			else
				throw new IllegalArgumentException("Bad hexadecimal digit");
			if ((c2 >= '0') && (c2 <= '9'))
				b += (c2 - '0');
			else if ((c2 >= 'a') && (c2 <= 'f'))
				b += (c2 - 'a' + 10);
			else if ((c2 >= 'A') && (c2 <= 'F'))
				b += (c2 - 'A' + 10);
			else
				throw new IllegalArgumentException("Bad hexadecimal digit");
			baos.write(b);
		}
		return (baos.toByteArray());
	}

	/**
	 * Convert a byte array into a printable format containing a
	 * String of hexadecimal digit characters (two per byte).
	 */
	private String convert(byte bytes[]) {
		StringBuffer sb = new StringBuffer(bytes.length * 2);
		for (int i = 0; i < bytes.length; i++) {
			sb.append(convertDigit((int) (bytes[i] >> 4)));
			sb.append(convertDigit((int) (bytes[i] & 0x0f)));
		}
		return (sb.toString());
	}
    
	/**
	 * Convert the specified value (0 .. 15) to the corresponding
	 * hexadecimal digit.
	 */
	private static char convertDigit(int value) {
		value &= 0x0f;
		if (value >= 10)
			return ((char) (value - 10 + 'a'));
		else
			return ((char) (value + '0'));
	}
    
    
	//-------------- Inner Type EncException ----------------
	/**
	 * Encryption Exception
	 */
	public class EncException extends Exception {

		/**
		 * Constructor for EncException.
		 */
		public EncException() {
			super();
		}

		/**
		 * Constructor for EncException.
		 * @param message
		 */
		public EncException(String message) {
			super(message);
		}

		/**
		 * Constructor for EncException.
		 * @param message
		 * @param cause
		 */
		public EncException(String message, Throwable cause) {
			super(message, cause);
		}

		/**
		 * Constructor for EncException.
		 * @param cause
		 */
		public EncException(Throwable cause) {
			super(cause);
		}

	}
}
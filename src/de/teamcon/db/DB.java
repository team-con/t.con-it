/*
 * Created on 15.03.2010
 *
 */
package de.teamcon.db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.util.ArrayList;

/**
 * @author koelbln
 *
 */
public class DB {
	
	final static ArrayList<String> errors = new ArrayList<String>();
	
	/** Liefert eine Datenbank-Connection (java.sql.Connection)
	 */    
	public static synchronized Connection getConnection() {
		
		String url = "jdbc:jtds:sqlserver://appgdbs01.tcon-group.de:3695/TconDB";
		String driver = "net.sourceforge.jtds.jdbc.Driver";
		String userName = "portaluser";
		String password = "fcbayern";
		Connection conn = null;
		try {
			Class.forName(driver);
			conn = DriverManager.getConnection(url, userName, password);
			conn.setTransactionIsolation(Connection.TRANSACTION_SERIALIZABLE);
			return conn;
		} catch (Exception e) {
			errors.add(e.getMessage());
			return null;
		}
	}
    
	/** schließt eine Datenbankverbindung bzw. gibt sie wieder frei
	 */    
	public static synchronized void closeConnection(Connection conn) {
		
		if (conn != null) {
			try {
				conn.close();
			}
			catch (Exception e) {
				errors.add(e.getMessage());
			}
		}
	}
	
	/**
	 * Liefert kuerzlich aufgetretene Fehler zurueck.
	 * @return alle Fehler als ein String.
	 */
	public static synchronized String getErrors() {
		StringBuilder bob = new StringBuilder("\nDB-Fehler:\n");
		for (String s : errors) {
			bob.append(" ").append(s);
		}
		if (errors.size() == 0) {
			bob.append(" keine");
		}
		errors.clear();
		return bob.toString();
	}
}

package de.teamcon.db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.util.ArrayList;

/**
 * @author feldmern
 *
 */
public class JiraDB {
	
	final static ArrayList<String> errors = new ArrayList<String>();
	
	/** 
	 * Returns data base connection (java.sql.Connection)
	 */    
	public static synchronized Connection getConnection() {
		
		String url = "jdbc:jtds:sqlserver://appgdbs01.tcon-group.de:3694/jiradb";
		String driver = "net.sourceforge.jtds.jdbc.Driver";
		String userName = "jirauser";
		String password = "zxJiey$FgOdIg!4g6";
		Connection conn = null;
		
		try {
			Class.forName(driver);
			conn = DriverManager.getConnection(url, userName, password);
			conn.setTransactionIsolation(Connection.TRANSACTION_SERIALIZABLE);
			return conn;
		} catch (Exception e) {
			errors.add(e.getMessage());
			return null;
		}
		
	}
    
	/** 
	 * Closes data base connection
	 */    
	public static synchronized void closeConnection(Connection conn) {
		
		if (conn != null) {
			try {
				conn.close();
			}
			catch (Exception e) {
				errors.add(e.getMessage());
			}
		}
		
	}
	
	/**
	 * Returns latest errors
	 * @return bob.toString()		latest errors
	 */
	public static synchronized String getErrors() {
		
		StringBuilder bob = new StringBuilder("\nDB-Fehler:\n");
		
		for (String s : errors) {
			bob.append(" ").append(s);
		}
		
		if (errors.size() == 0) {
			bob.append(" keine");
		}
		
		errors.clear();
		
		return bob.toString();
		
	}
	
}

package de.teamcon.email;

import java.io.IOException;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

/**
 * @author feldmern
 */
public class Email {

	private static final String SERVER_ADRESS = "mail.tcon-group.de";

	/**
	 * Sends mail to "Support.IT@team-con.de" with issue key as subject.
	 * Mail content will be added to ticket as a comment.
	 * 
	 * @param subject		mail subject (jira ticket key)
	 * @param body 			mail content (jira ticket comment)
	 * @return true 		if message was sent, false if message was not sent
	 * @throws IOException	if FileHandler could not be added to Logger
	 */
	public static boolean sendMail(String subject, String body) throws IOException {
		
		String to = "support.it@team-con.de";
//		String cc = "nicola.feldmer@team-con.de";
		String from = "automation.service@team-con.de";
		
		Properties properties = System.getProperties();
		properties.setProperty("mail.smtp.host", SERVER_ADRESS);
		
		Session session = Session.getDefaultInstance(properties);
		
		try {
			MimeMessage message = new MimeMessage(session);
			message.setFrom(new InternetAddress(from));
			message.addRecipient(Message.RecipientType.TO, new InternetAddress(to));
//			message.addRecipient(Message.RecipientType.TO, new InternetAddress(cc));
			message.setSubject(subject);
			message.setText(body);
			
			Transport.send(message);
			
			return true;
			
		} catch (MessagingException e) {
			return false;
		}
	}
}
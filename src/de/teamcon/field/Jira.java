package de.teamcon.field;

public enum Jira {

	TYP("issuetype"),
	SIGNATUR1("customfield_12200"), //dd
	SIGNATUR2("customfield_12201"), //normal
	NACHNAME("customfield_11504"), //normal
	VORNAME("customfield_11503"), //n
	ORT("customfield_11627"), //n
	STANDARD_SERVICE("customfield_11624"), //n
	FIRMA("customfield_11505"), //dd
	FUNKTION("customfield_11620"), //n
	ABTEILUNG("customfield_11619"), //dd
	EINTRITTSDATUM("customfield_11700"), //n
	VORGESETZTER("customfield_11621"), //benutzer
	CPU("customfield_12902"),
	KUNDE("customfield_12900"),
	INSTALLATION("customfield_12909"),
	SYSTEMART("customfield_11608"),
	SERVERNAME("customfield_12908"),
	HDD2("customfield_12905"),
	HDD3("customfield_12906"),
	HDD4("customfield_12907"),
	OS("customfield_11607"),
	RAM("customfield_12903"),
	ZWECK("customfield_13001"),
	HV("customfield_11615"),
	SYSTEM("customfield_11611"),
	KOSTENSTELLE("customfield_12000"),
	IMPLEMENTIERT("customfield_13724"),
	QUELLE("customfield_11305"),
	FWPORT("customfield_11304"),
	FIRMA_NEU("customfield_13726")
	;

	private String jiraField;

	private Jira(final String jiraField) {
		this.jiraField = jiraField;
	}

	public String getField() {
		return jiraField;
	}
}
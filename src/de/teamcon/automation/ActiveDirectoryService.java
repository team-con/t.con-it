package de.teamcon.automation;

import java.util.Hashtable;

import javax.naming.AuthenticationException;
import javax.naming.AuthenticationNotSupportedException;
import javax.naming.Context;
import javax.naming.NamingException;
import javax.naming.directory.BasicAttribute;
import javax.naming.directory.DirContext;
import javax.naming.directory.InitialDirContext;
import javax.naming.directory.ModificationItem;
import javax.ws.rs.POST;
import javax.ws.rs.Path;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * @author feldmern
 * Access path: "/Automation/rest/ad/..."
 */
@Path("ad")
public class ActiveDirectoryService {

	private static final String COMPUTERNAME = "computername";
	private static final String USERNAME = "username";
	private static final String DOMAIN = "domain";
	private static final String TCON = "tcon.tcon-group.de";
	private static final String TSERV = "tserv.tcon-group.de";
	private static final String[] USERS = {
		"tcon\\ldapuser",
		"ts\\ldapuser"
	};
	private static final String[] PASSWORDS = {
		"BzyK73Nj5T1l6FsSmT5v",
		"tI+r8AQZU%/q8a8ii@SH"
	};
	private static final String[] PROVIDERURLS = {
		"ldaps://DOMI1DC01.tcon.tcon-group.de:636",
		"ldaps://DOMI2DC01.tserv.tcon-group.de:636"
	};
	
	@POST
	@Path("update/computer")
	public void postActionUpdateComputerObject(final String json) throws JSONException, NamingException {
		
		JSONObject jsonObject = new JSONObject(json);
		String computername = jsonObject.getString(COMPUTERNAME);
		String username = jsonObject.getString(USERNAME);
		String domain = jsonObject.getString(DOMAIN).toLowerCase();
		String dn = "";
		
		if (TCON.equals(domain)) {
			
			dn = "CN=" + computername + ",OU=Windows 10,OU=_Computer,DC=tcon,DC=tcon-group,DC=de";
			DirContext ctx = getADConnection(0);
			
			BasicAttribute basicAttribute = new BasicAttribute("description", username);
			ModificationItem[] item = new ModificationItem[1];
			item[0] = new ModificationItem(DirContext.REPLACE_ATTRIBUTE, basicAttribute);
			ctx.modifyAttributes(dn, item);
			
		} else if (TSERV.equals(domain)) {
			
			dn = "CN=" + computername + ",OU=Windows 10,OU=_Computer,DC=tserv,DC=tcon-group,DC=de";
			DirContext ctx = getADConnection(1);
			
			BasicAttribute basicAttribute = new BasicAttribute("description", username);
			ModificationItem[] item = new ModificationItem[1];
			item[0] = new ModificationItem(DirContext.REPLACE_ATTRIBUTE, basicAttribute);
			ctx.modifyAttributes(dn, item);
		}
	}
	
	private DirContext getADConnection(int companyNumber) {
		
		DirContext userContext = null;
		
		Hashtable<String, Object> env = new Hashtable<String, Object>();
	    env.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
	    env.put(Context.PROVIDER_URL, PROVIDERURLS[companyNumber]);
	    env.put(Context.SECURITY_PRINCIPAL, USERS[companyNumber]);
	    env.put(Context.SECURITY_CREDENTIALS, PASSWORDS[companyNumber]);
	    env.put(Context.SECURITY_AUTHENTICATION, "simple");
	    env.put("java.naming.ldap.attributes.binary", "objectSID objectGUID");
	    
	    try {
	    	userContext = new InitialDirContext(env);
	    } catch (AuthenticationNotSupportedException ex) {
	        System.out.println("The authentication is not supported by the server");
	    } catch (AuthenticationException ex) {
	        System.out.println("incorrect password or username");
	    } catch (NamingException ex) {
	        System.out.println("error when trying to create the context" + ex);
	    }
		
		return userContext;
		
	}
	
}

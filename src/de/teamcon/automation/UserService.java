package de.teamcon.automation;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.X509Certificate;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.Types;
import java.util.HashMap;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.MediaType;

import org.json.JSONException;
import org.json.JSONObject;

import de.teamcon.db.DB;
import de.teamcon.encryption.Encryption;
import de.teamcon.field.Jira;
import de.teamcon.powershell.PowerShell;

/**
 * @author feldmern
 * Access path: "/Automation/rest/user/..."
 */
@Path("user")
public class UserService {

	/**
	 * Executes PowerShell script to create new user in Acitve Directory.
	 * User information provided by jira webhook.
	 * Comments PowerShell output to ticket with the given key.
	 * 
	 * @param key						jira ticket key
	 * @param json						json sent by jira webhook
	 * @throws IOException				if FileHandler could not be added to Logger
	 */
	@POST
	@Path("new/{key}")
	public void postActionNewEmployee(@PathParam("key") final String key, String json) throws IOException {
		
		if (key.indexOf("MAPROZ") >= 0) {
			
			String parentKey = getParentKey(key);
			json = getJsonFromIssue(parentKey);
			
		} 
		
		try {
			String g1 = "null";
			String g2 = "null";
			String g3 = "null";
			String g4 = "null";
			String g5 = "null";
			String ouPath = "";
			String signatur = "";
			String str = "";
			String command = "";
			
			JSONObject jsonobj = new JSONObject(json);

			HashMap<Jira, String> found = new HashMap<Jira, String>();
			for (Jira jira : Jira.values()) {
				try {
					JSONObject fields = null;
					
					if (key.indexOf("MAPROZ") < 0) {
						JSONObject issue = (JSONObject) jsonobj.get("issue");
						fields = (JSONObject) issue.get("fields");
					} else {
						fields = (JSONObject) jsonobj.get("fields");
					}
					
					Object temp = fields.get(jira.getField());
					if (temp != null) {
						try {
							JSONObject temp2 = (JSONObject) temp;
							try {
								found.put(jira, temp2.getString("value"));
							} catch (Exception e) {
								try {
									found.put(jira, temp2.getString("name"));
								} catch (Exception e1) {
									found.put(jira, temp2.getString("displayName"));
								}
							}
						} catch (Exception e) {
							found.put(jira, temp + "");
						}
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			
			String abteilung = found.get(Jira.ABTEILUNG);
			String firma = found.get(Jira.FIRMA);
			
			if (firma == null) {
				firma = found.get(Jira.FIRMA_NEU);
			}
			
			String position = found.get(Jira.FUNKTION);
			String nachname = found.get(Jira.NACHNAME);
			String ort = found.get(Jira.ORT).split(" ")[0];
			String eintrittsdatum = found.get(Jira.EINTRITTSDATUM);
			
			if (nachname.endsWith(" ")) {
				nachname.substring(0, nachname.length() - 1);
			}
			
			if (found.get(Jira.SIGNATUR2).length() == 0 || found.get(Jira.SIGNATUR2).equals("null")) {
				signatur = found.get(Jira.SIGNATUR1);
			} else {
				signatur = found.get(Jira.SIGNATUR1) + " | " + found.get(Jira.SIGNATUR2);
			}
			
			String vorgesetzter = found.get(Jira.VORGESETZTER);
			String vorname = found.get(Jira.VORNAME).replace(" ", "");
			String sam = nachname.toLowerCase() + vorname.toLowerCase().charAt(0);
			sam = sam.replace("Ä", "Ae").replace("ä", "ae").replace("Ö", "Oe").replace("ö", "oe").replace("Ü", "Ue").replace("ü", "ue").replace("ß", "ss").replace(" ", "");
			String email = vorname + "." + nachname;
			email = email.replace("Ä", "Ae").replace("ä", "ae").replace("Ö", "Oe").replace("ö", "oe").replace("Ü", "Ue").replace("ü", "ue").replace("ß", "ss").replace(" ", "");
			String firmaNr = "1";
			String standortNr = "1";
			String passwortCrypt = "";
			
			if (firma.equals("T.CON")) {
				
				passwortCrypt = new Encryption("MD5").encryptHex("TCON@neu01!").toString();
				
				if (!ort.equals("Plattling")) {
					firmaNr = "282";
					standortNr = "3";
				}
				
				email += "@team-con.de";
				g1 = "SG_Portal_TCON";
				ouPath = "OU=Benutzeranlage,OU=Internal,OU=_User,DC=tcon,DC=tcon-group,DC=de";
				
			} else if (firma.equals("T.SERV") || firma.equals("t.serv")){
				
				passwortCrypt = new Encryption("MD5").encryptHex("TSERV@neu01!").toString();

				g1 = "SG_Portal_TSERV";
				ouPath = "OU=Internal,OU=_User,DC=tserv,DC=tcon-group,DC=de";
				
				if (abteilung.startsWith("BPO")) {
					
					g2 = "SG_Portal_MPS_" + ort;
					ouPath = "OU=" + ort + ",OU=MPS," + ouPath;
					
				}
				
				if (ort.equals("Plattling")) {
					firmaNr = "3";
					email += "@tserv.de";
				} else if (ort.equals("Papenburg")) {
					firmaNr = "384";
					standortNr = "16";
					email += "@tserv.de";
				} else {
					firmaNr = "392";
					standortNr = "17";
					email += "@tserv.at";
				}
				
			} else {
				
				ouPath = "OU=Internal,OU=_User,DC=factore,DC=tcon-group,DC=de";
				
			}
			
			StringBuilder bob = new StringBuilder();
			bob.append("C:\\Windows\\System32\\WindowsPowerShell\\v1.0\\powershell.exe -noexit -command \"C:\\Automation\\Scripts\\User\\NewEmployee.ps1");
			bob.append(" '"+ ouPath + "'");
			bob.append(" '"+ sam + "'");
			bob.append(" '"+ vorname + "'");
			bob.append(" '"+ nachname + "'");
			bob.append(" '"+ signatur + "'");
			bob.append(" '"+ position + "'");
			bob.append(" '"+ abteilung + "'");
			bob.append(" '"+ vorgesetzter + "'");
			bob.append(" '"+ g1 + "'");
			bob.append(" '"+ g2 + "'");
			bob.append(" '"+ g3 + "'");
			bob.append(" '"+ g4 + "'");
			bob.append(" '"+ g5 + "'");
			bob.append(" '"+ key + "'");
			bob.append(" '"+ email + "'");
			bob.append("\"");
			
			command = bob.toString();
			
			if (firma.equals("T.CON") || firma.equals("T.SERV") || firma.equals("t.serv")) {
				insertPortalUser(firmaNr, vorname, nachname, sam, passwortCrypt, email, eintrittsdatum, standortNr);
			}
			
			str = PowerShell.executePS(Runtime.getRuntime(), str, command);
					
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	/**
	 * Executes PowerShell script to disable user in Acitve Directory.
	 * User information provided by jira webhook.
	 * Comments PowerShell output to ticket with the given key.
	 * 
	 * @param key						jira ticket key
	 * @param json						json sent by jira webhook
	 * @throws IOException				if FileHandler could not be added to Logger
	 * @throws JSONException			if Error while parsing JSON
	 * @throws InterruptedException		if Error while getting runtime
	 */
	@POST
	@Path("disable/{key}")
	@Consumes(MediaType.APPLICATION_JSON)
	public void postActionDisableEmployee(@PathParam("key") final String key, final String json) throws IOException, JSONException, InterruptedException {
        
        String str = "";
		
		JSONObject jsonobj = new JSONObject(json);

		HashMap<Jira, String> found = new HashMap<Jira, String>();
		for (Jira jira : Jira.values()) {
			try {
				JSONObject issue = (JSONObject) jsonobj.get("issue");
				JSONObject fields = (JSONObject) issue.get("fields");
				
				Object temp = fields.get(jira.getField());
				if (temp != null) {
					try {
						JSONObject temp2 = (JSONObject) temp;
						try {
							found.put(jira, temp2.getString("value"));
						} catch (Exception e) {
							try {
								found.put(jira, temp2.getString("name"));
							} catch (Exception e1) {
								found.put(jira, temp2.getString("displayName"));
							}
						}
					} catch (Exception e) {
						found.put(jira, temp + "");
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		String sam = found.get(Jira.NACHNAME).toLowerCase() + found.get(Jira.VORNAME).substring(0,1).toLowerCase();
		String mail = getMailFromNtuser(found.get(Jira.VORGESETZTER));
		
		StringBuilder bob = new StringBuilder();
		bob.append("C:\\Windows\\System32\\WindowsPowerShell\\v1.0\\powershell.exe -noexit -command \"C:\\Automation\\Scripts\\User\\DisableUser.ps1");
		bob.append(" '"+ sam + "'");
		bob.append(" '"+ mail + "'");
		bob.append("\"");
		
		String command = bob.toString();
		
		str = PowerShell.executePS(Runtime.getRuntime(), str, command);
		
	}

	/**
	 * Executes PowerShell script to disable user in Acitve Directory.
	 * User information provided by jira webhook.
	 * Comments PowerShell output to ticket with the given key.
	 * 
	 * @param key						jira ticket key
	 * @param json						json sent by jira webhook
	 * @throws IOException				if FileHandler could not be added to Logger
	 * @throws JSONException			if Error while parsing JSON
	 * @throws InterruptedException		if Error while getting runtime
	 */
	@POST
	@Path("move/{key}")
	@Consumes(MediaType.APPLICATION_JSON)
	public void postActionMoveEmployee(@PathParam("key") final String key, final String json) throws IOException, JSONException, InterruptedException {
		
        String str = "";
		
		JSONObject jsonobj = new JSONObject(json);

		HashMap<Jira, String> found = new HashMap<Jira, String>();
		for (Jira jira : Jira.values()) {
			try {
				JSONObject issue = (JSONObject) jsonobj.get("issue");
				JSONObject fields = (JSONObject) issue.get("fields");
				
				Object temp = fields.get(jira.getField());
				if (temp != null) {
					try {
						JSONObject temp2 = (JSONObject) temp;
						try {
							found.put(jira, temp2.getString("value"));
						} catch (Exception e) {
							try {
								found.put(jira, temp2.getString("name"));
							} catch (Exception e1) {
								found.put(jira, temp2.getString("displayName"));
							}
						}
					} catch (Exception e) {
						found.put(jira, temp + "");
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		String nachname = found.get(Jira.NACHNAME).toLowerCase().replace(" ", "");
		String vorname = found.get(Jira.VORNAME).toLowerCase().replace(" ", "");
		String abteilung = found.get(Jira.ABTEILUNG);
		String firma = found.get(Jira.FIRMA);
		String ort = found.get(Jira.ORT);
		
		if (ort.split(" ").length > 1) {
			ort = ort.split(" ")[0];
		}
		
		String targetPath = "";
		
		if (firma.equals("T.CON")) {
			
			targetPath = "OU=Internal,OU=_User,DC=tcon,DC=tcon-group,DC=de";
			
			if (abteilung.startsWith("ALM")) {

				targetPath = "OU=" + abteilung + ",OU=GB_ALM," + targetPath;
				
			} else if (abteilung.startsWith("BI")) {

				targetPath = "OU=" + abteilung + ",OU=GB_BI," + targetPath;
				
			} else if (abteilung.startsWith("Customer")) {

				targetPath = "OU=GB_CUS," + targetPath;
				
			} else if (abteilung.startsWith("ERP")) {

				targetPath = "OU=" + abteilung + ",OU=GB_ERP," + targetPath;
				
			} else if (abteilung.startsWith("MES")) {

				targetPath = "OU=" + abteilung + ",OU=GB_MES," + targetPath;
				
			} else if (abteilung.startsWith("Geb")) {

				targetPath = "OU=GB_Facility Mgmt," + targetPath;
				
			} else if (abteilung.equals("IT")) {
				
				targetPath = "OU=GB_ITA," + targetPath;
					
			} else if (abteilung.equals("Personal")) {
				
				targetPath = "OU=HR,OU=GB_HCM," + targetPath;
				
			} else if (abteilung.equals("HCM onPrem")) {
				
				targetPath = "OU" + abteilung + ",OU=GB_HCM," + targetPath;
				
			} else if (abteilung.equals("HR Cloud")) {
				
				targetPath = "OU" + abteilung + ",OU=GB_HCM," + targetPath;
				
			} else {
				
				targetPath = "OU=GB_Administration," + targetPath;
					
			}
				
		} else {

			targetPath = "OU=Internal,OU=_User,DC=tserv,DC=tcon-group,DC=de";
			
			if (abteilung.startsWith("BPO")) {
				
				targetPath = "OU=" + ort + ",OU=MPS," + targetPath;
				
			}
		}
		
		String sam = nachname + vorname.substring(0,1);
		
		StringBuilder bob = new StringBuilder();
		bob.append("C:\\Windows\\System32\\WindowsPowerShell\\v1.0\\powershell.exe -noexit -command \"C:\\Automation\\Scripts\\User\\MoveUser.ps1");
		bob.append(" '"+ sam + "'");
		bob.append(" '"+ targetPath + "'");
		bob.append("\"");
		
		String command = bob.toString();
		
		str = PowerShell.executePS(Runtime.getRuntime(), str, command);
		
	}
	
	private String getMailFromNtuser(String ntuser) {
		String mail = "";
		
		Connection conn = null;
		String sql = "SELECT email FROM person WHERE ntuser LIKE '" + ntuser + "'";
		
		try {
			
			conn = DB.getConnection();
			Statement stmt = conn.createStatement();
			
			ResultSet rs = stmt.executeQuery(sql);
			
			while(rs.next()) {
				mail = rs.getString("email");
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			DB.closeConnection(conn);
		}
		
		return mail;
	}
	
	private void insertPortalUser(String firmaNr, String vorname, String nachname, String ntuser, String passwortCrypt, String eMail, String eintrittsdatum, String standortNr) {
		
		Connection conn = null;
		String sqlInsert = "INSERT INTO dbo.person (firma_nr, restricted_projekt_nr, vorname, nachname, ntuser, passwort, geburtstag, email, eintrittsdatum, standort_nr) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
		
		try {
			
			conn = DB.getConnection();
			PreparedStatement psInsert = conn.prepareStatement(sqlInsert);
			
			psInsert.setString(1, firmaNr);
			psInsert.setString(2, "0");
			psInsert.setString(3, vorname);
			psInsert.setString(4, nachname);
			psInsert.setString(5, ntuser);
			psInsert.setString(6, passwortCrypt);
			psInsert.setString(7, "");					// Geburtstag
			psInsert.setString(8, eMail);
			if (eintrittsdatum != null) {psInsert.setString(9, eintrittsdatum);} else {psInsert.setNull(9, Types.VARCHAR);}
			psInsert.setString(10, standortNr);

			psInsert.executeUpdate();
			
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			DB.closeConnection(conn);
		}
		
	}
	
	private String getJsonFromIssue(final String key) {
		
		String json = null;
		StringBuilder bob = new StringBuilder();
		
		try {
			
			X509TrustManager x509TrustManager = new X509TrustManager() {
                public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                	return null;
                }
                public void checkClientTrusted(X509Certificate[] certs, String authType) {
                	//nix
                }
                public void checkServerTrusted(X509Certificate[] certs, String authType) {
                	//nix
                }
            };
            
            TrustManager[] trustAllCerts = new TrustManager[] {x509TrustManager};
	 
	        SSLContext sc = SSLContext.getInstance("SSL");
	        sc.init(null, trustAllCerts, new java.security.SecureRandom());
	        HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
	 
	        HostnameVerifier allHostsValid = new HostnameVerifier() {
	            public boolean verify(String hostname, SSLSession session) {
	                return true;
	            }
	        };
	 
	        HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);
			
			String resturl = "https://jira.team-con.de/rest/api/2/issue/" + key;
			
			URL url = new URL(resturl);
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setRequestMethod("GET");
			conn.setRequestProperty("Content-Type", "application/json");
			conn.setRequestProperty("Authorization", "Basic emVfdXNlcjp3N1JRcGw3YkQlLUpRdjMmIjloYg==");
			
			if (conn.getResponseCode() != 200) {
				throw new RuntimeException("Failed: HTTP error code: " + conn.getResponseCode());
			}
			
			BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"));
			
			String output;
			while ((output = br.readLine()) != null) {
				bob.append(output);
			}
			
			conn.disconnect();
		
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		} catch (KeyManagementException e) {
			e.printStackTrace();
		}
		
		json = bob.toString();
		
		return json;
		
	}
	
	private String getParentKey(final String key) {
		
		String parentKey = "";
		
		StringBuilder bob = new StringBuilder();
		
		try {
			
			X509TrustManager x509TrustManager = new X509TrustManager() {
                public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                	return null;
                }
                public void checkClientTrusted(X509Certificate[] certs, String authType) {
                	//nix
                }
                public void checkServerTrusted(X509Certificate[] certs, String authType) {
                	//nix
                }
            };
            
            TrustManager[] trustAllCerts = new TrustManager[] {x509TrustManager};
	 
	        SSLContext sc = SSLContext.getInstance("SSL");
	        sc.init(null, trustAllCerts, new java.security.SecureRandom());
	        HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
	 
	        HostnameVerifier allHostsValid = new HostnameVerifier() {
	            public boolean verify(String hostname, SSLSession session) {
	                return true;
	            }
	        };

	        HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);
			
			String resturl = "https://jira.team-con.de/rest/api/2/issue/" + key;
			
			URL url = new URL(resturl);
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setRequestMethod("GET");
			conn.setRequestProperty("Content-Type", "application/json");
			conn.setRequestProperty("Authorization", "Basic emVfdXNlcjp3N1JRcGw3YkQlLUpRdjMmIjloYg==");
			
			if (conn.getResponseCode() != 200) {
				throw new RuntimeException("Failed: HTTP error code: " + conn.getResponseCode());
			}
			
			BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"));
			
			String output;
			while ((output = br.readLine()) != null) {
				bob.append(output);
			}
			
			conn.disconnect();
		
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		} catch (KeyManagementException e) {
			e.printStackTrace();
		}
		
		JSONObject jsonObject;
		try {
			jsonObject = new JSONObject(bob.toString());
			JSONObject fields = (JSONObject) jsonObject.get("fields");
			JSONObject parent = (JSONObject) fields.get("parent");
			parentKey = parent.getString("key");
		} catch (JSONException e) {
			e.printStackTrace();
		}
		
		return parentKey;
		
	}
	
}

package de.teamcon.automation;

import java.net.URI;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.ws.rs.POST;
import javax.ws.rs.Path;

import org.json.JSONObject;

import microsoft.exchange.webservices.data.core.ExchangeService;
import microsoft.exchange.webservices.data.core.enumeration.misc.ExchangeVersion;
import microsoft.exchange.webservices.data.core.enumeration.property.WellKnownFolderName;
import microsoft.exchange.webservices.data.core.service.item.Appointment;
import microsoft.exchange.webservices.data.credential.ExchangeCredentials;
import microsoft.exchange.webservices.data.credential.WebCredentials;
import microsoft.exchange.webservices.data.property.complex.FolderId;
import microsoft.exchange.webservices.data.property.complex.Mailbox;

/**
 * @author feldmern
 * Access path: "/Automation/rest/vacation/..."
 */
@Path("vacation")
public class VacationService {

	ExchangeService service;
	
	@POST
	@Path("new")
	public void postAction(String json) {
	
		JSONObject jsonobj = null;
		String email = "";
		Date start = null;
		Date end = null;
		
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		
		try {
			
			jsonobj = new JSONObject(json);
			
			email = jsonobj.getString("email");
			start = formatter.parse(jsonobj.getString("start"));
			end = formatter.parse(jsonobj.getString("end"));
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		try {
		
			service = new ExchangeService(ExchangeVersion.Exchange2010_SP2);
			ExchangeCredentials credentials = new WebCredentials("teamkalender2010", "TCON@team2010!");
			service.setCredentials(credentials);
			URI url = new URI("https://mail.team-con.de/ews/exchange.asmx");
			service.setUrl(url);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		Mailbox mailbox = new Mailbox(email);
		FolderId folderId = new FolderId(WellKnownFolderName.Calendar, mailbox);
		
		try {
			
			Appointment app = new Appointment(service);
			app.setSubject("Urlaub");
			app.setStart(start);
			app.setEnd(end);
			app.save(folderId);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	
	}
	
}

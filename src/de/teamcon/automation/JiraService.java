package de.teamcon.automation;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.X509Certificate;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.Types;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.google.gson.Gson;

import de.teamcon.db.DB;
import de.teamcon.email.Email;

/**
 * @author feldmern
 * Access path: "/Automation/rest/jira/..."
 */
@Path("jira")
public class JiraService {
	
	private static final String SERVER_ADRESS = "mail.tcon-group.de";
	
	/************************************
	 * 			JIRA CLOUD				*
	 ************************************/
	
	/**
	 * @param key
	 * @throws IOException
	 */
	@POST
	@Path("sendNote/{key}")
	public void sendTeamsNotifiction(@PathParam("key") final String key) throws IOException {
		
		String subject = "Issue " + key + " has been closed";
		String body = "Issue URL: https://team-con.atlassian.net/browse/" + key;
		
		String to = "nicola.feldmer@team-con.de";
		String from = "noreply@team-con.de";
		
		Properties properties = System.getProperties();
		properties.setProperty("mail.smtp.host", SERVER_ADRESS);
		
		Session session = Session.getDefaultInstance(properties);
		
		try {
			MimeMessage message = new MimeMessage(session);
			message.setFrom(new InternetAddress(from));
			message.addRecipient(Message.RecipientType.TO, new InternetAddress(to));
			message.setSubject(subject);
			message.setText(body);
			
			Transport.send(message);
			
		} catch (MessagingException e) {
			e.printStackTrace();
		}
		
	}
	
	
	
	/************************************
	 * 			JIRA SERVER				*
	 ************************************/
	
	/**
	 * @param key
	 * @return
	 * @throws JSONException
	 */
	@GET
	@Path("issue/{key}")
	@Produces(MediaType.APPLICATION_JSON)
	public String getIssueJSON(@PathParam("key") final String key) throws JSONException {
		
		HashMap <String, String> map = new HashMap<String, String>();
		
		String projectkey = key.split("-")[0];
		String projectTimeTrackingRelevant = isProjectTimeTrackingRelevant(projectkey);
		
		if (projectTimeTrackingRelevant.equals("nicht vorhanden")) {
			map.put("timeTrackingAllowed", "true");
		} else {

			String issueTimeTrackingRelevant = isIssueTimeTrackingRelevant(key);
			
			if(projectTimeTrackingRelevant.equals("true") && issueTimeTrackingRelevant.equals("true")) {
				map.put("timeTrackingAllowed", "true");
			} else if(projectTimeTrackingRelevant.equals("true") && issueTimeTrackingRelevant.equals("false")) {
				map.put("timeTrackingAllowed", "false");
			} else if(projectTimeTrackingRelevant.equals("true") && issueTimeTrackingRelevant.equals("nicht vorhanden")) {
				map.put("timeTrackingAllowed", "false");
			} else if(projectTimeTrackingRelevant.equals("false") && issueTimeTrackingRelevant.equals("true")) {
				map.put("timeTrackingAllowed", "false");
			} else if(projectTimeTrackingRelevant.equals("false") && issueTimeTrackingRelevant.equals("false")) {
				map.put("timeTrackingAllowed", "false");
			} else if(projectTimeTrackingRelevant.equals("false") && issueTimeTrackingRelevant.equals("nicht vorhanden")) {
				map.put("timeTrackingAllowed", "false");
			}
			
		}
		
		return new Gson().toJson(map);
	}
	
	/**
	 * @param key
	 * @param transitionId
	 */
	@POST
	@Path("issue/{key}/dotransition/{transitionId}")
	public void closeParent(@PathParam("key") final String key, @PathParam("transitionId") final int transitionId) {
		
		String parentKey = getParentIssue(key);
		
		if (areSubtasksClosed(parentKey)) {
			
			doTransition(parentKey, transitionId);
			
		}
		
	}
	
	/**
	 * @param key
	 */
	@POST
	@Path("issue/{key}/syncComments")
	public void syncComments(@PathParam("key") final String key) {
		
		ArrayList<String> keys = new ArrayList<String>();
		HashMap<String, String> lastComment = getLastComment(key);
		
		String comment = "<b>" + lastComment.get("authorName") + " sagt in " + key + ":</b><br/>" + lastComment.get("body");
		
		if (isParent(key)) {
			keys = getSubtaskKeys(key);
		} else {
			String parentKey = getParentIssue(key);
			keys = getSubtaskKeys(parentKey);
			keys.add(parentKey);
			int index = keys.indexOf(key);
			keys.remove(index);
		}
		
		if (!"Kommentarsync".equals(lastComment.get("authorName"))) {
		
			for(String skey : keys) {
				addComment(skey, comment);
			}
		
		}
		
	}
	
	/**
	 * @param key
	 */
	@POST
	@Path("issue/{key}/enableLinkedSAPTicket")
	public void enableLinkedSAPTicket(@PathParam("key") final String key) {
		
		String[] issue = key.split("-");
		int count = Integer.parseInt(issue[issue.length - 1]);
		count++;
		String linkedSAPTicketKey = "MAPROZ" + count;
		
//		final String parentKey = getParentIssue(key);
//		ArrayList<String> subtaskKeys = getSubtaskKeys(parentKey);
//		String linkedSAPTicketKey = subtaskKeys.get(subtaskKeys.size() - 1);
		
		// Transition-ID 41 = Übergang "Initialisieren" in Workflow "MAPROZ - Eintritt Interner MA - Teilaufgabe SAP Basis"
		doTransition(linkedSAPTicketKey, 41);
		
	}
	
	/**
	 * 
	 * Returns tracked time for a specific issue depending on ticket key
	 * 
	 * @param 		ticketkey	Issue key, e.g. TCITSUP-1234
	 * @return 		JSON string with tracked time
	 */
	@GET
	@Path("issue/{key}/getTimeTracked")
	public String getTimeTrackedByKey(@PathParam("key") final String ticketkey) {
		
		String jsonSrc = "";
		double timeTracked = 0.0;
		StringBuilder bob = new StringBuilder();
		
		try {
			//Umstellung am 10.12.2019 : https://sap-tep.team-con.de/  --- gw.team-con.de
			String resturl = "https://sap-tep.team-con.de/sap/opu/odata/TCON/ZE_SERVICES_SRV/TimeCollection?$filter=substringof(%27" + ticketkey +"%27,Filter)%20and%20(Workdate%20ge%20datetime%271998-01-01T10%3a00%3a00%27%20and%20Workdate%20le%20datetime%279999-12-31T10%3a00%3a00%27)&$format=json";
			
			URL url = new URL(resturl);
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setRequestMethod("GET");
			conn.setRequestProperty("Content-Type", "application/json");
			// Authorization mit User 'ZUFALLR'
			//conn.setRequestProperty("Authorization", "Basic WlVGQUxMUjpTdGFydDEyMyM="); // TGP
			conn.setRequestProperty("Authorization", "Basic WlVGQUxMUjpTdGFydDEyMyM="); // TEP
			
			if (conn.getResponseCode() != 200) {
				throw new RuntimeException("Failed: HTTP error code: " + conn.getResponseCode());
			}
			
			BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"));
			
			String output;
			while ((output = br.readLine()) != null) {
				bob.append(output);
			}
			
			conn.disconnect();
		
		} catch (MalformedURLException e) {
		
			e.printStackTrace();
		
		} catch (IOException e) {
		
			e.printStackTrace();
		
		}
		
		JSONObject jsonObject = null;
		
		try {
			jsonObject = new JSONObject(bob.toString());
			JSONObject d = (JSONObject) jsonObject.get("d");
			JSONArray results = (JSONArray) d.get("results");
			
			for (int i = 0; i < results.length(); i++) {
				
				JSONObject time = (JSONObject) results.get(i);
				String beMot = time.getString("Bemot");
				if ("Z1".equals(beMot) || "Z2".equals(beMot)) {
					double timeTemp = time.getDouble("Catshours");
					timeTracked += timeTemp;
				}
				
			}
			
			jsonSrc = "" + timeTracked;
			
		} catch (Exception e) {
			jsonSrc = "Error: " + e.getMessage();
		}
		
		return new Gson().toJson(jsonSrc);
		
	}
	
	/**
	 * 
	 * Returns tracked time for a specific issue depending on ticket key for internal
	 * 
	 * @param 		ticketkey	Issue key, e.g. TCITSUP-1234
	 * @return 		JSON string with tracked time
	 */
	@GET
	@Path("issue/{key}/getTimeTrackedint")
	public String getTimeTrackedByKeyint(@PathParam("key") final String ticketkey) {
		
		String jsonSrc = "";
		double timeTracked = 0.0;
		StringBuilder bob = new StringBuilder();
		
		try {
			//Umstellung am 10.12.2019 : https://sap-tep.team-con.de/  --- gw.team-con.de
			String resturl = "https://sap-tep.team-con.de/sap/opu/odata/TCON/ZE_SERVICES_SRV/TimeCollection?$filter=substringof(%27" + ticketkey +"%27,Filter)%20and%20(Workdate%20ge%20datetime%271998-01-01T10%3a00%3a00%27%20and%20Workdate%20le%20datetime%279999-12-31T10%3a00%3a00%27)&$format=json";
			
			URL url = new URL(resturl);
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setRequestMethod("GET");
			conn.setRequestProperty("Content-Type", "application/json");
			// Authorization mit User 'ZUFALLR'
			//conn.setRequestProperty("Authorization", "Basic WlVGQUxMUjpTdGFydDEyMyM="); // TGP
			conn.setRequestProperty("Authorization", "Basic WlVGQUxMUjpTdGFydDEyMyM="); // TEP
			
			if (conn.getResponseCode() != 200) {
				throw new RuntimeException("Failed: HTTP error code: " + conn.getResponseCode());
			}
			
			BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"));
			
			String output;
			while ((output = br.readLine()) != null) {
				bob.append(output);
			}
			
			conn.disconnect();
		
		} catch (MalformedURLException e) {
		
			e.printStackTrace();
		
		} catch (IOException e) {
		
			e.printStackTrace();
		
		}
		
		JSONObject jsonObject = null;
		
		try {
			jsonObject = new JSONObject(bob.toString());
			JSONObject d = (JSONObject) jsonObject.get("d");
			JSONArray results = (JSONArray) d.get("results");
			
			for (int i = 0; i < results.length(); i++) {
				
				JSONObject time = (JSONObject) results.get(i);
				String beMot = time.getString("Bemot");
				if ("Z6".equals(beMot) || "Z7".equals(beMot)) {
					double timeTemp = time.getDouble("Catshours");
					timeTracked += timeTemp;
				}
				
			}
			
			jsonSrc = "" + timeTracked;
			
		} catch (Exception e) {
			jsonSrc = "Error: " + e.getMessage();
		}
		
		return new Gson().toJson(jsonSrc);
		
	}
	
	@POST
	@Path("firewall/createPolicy/{key}")
	public void createFWRule(@PathParam("key") final String key) throws JSONException {
		
		JSONObject issue = getTicketInfo(key);
		JSONObject fields = (JSONObject) issue.get("fields");
		
		// Custom Field "Implementiert auf"
		JSONObject cf13724 = (JSONObject) fields.get("customfield_13724");
		String implemented = cf13724.getString("value");
		
		if ("Hartl-EDV".equals(implemented)) {
		
			// Custom Field Quelle / customfield_11305
			String sourceIp = fields.getString("customfield_11305");
			
			// Custom Field Ziel / customfield_11306
			String destIp = fields.getString("customfield_11306");

			// Custom Field Ports / customfield_11304
			String ports = fields.getString("customfield_11304");
			
			String policyService = ports.replaceAll(",", " +").replaceAll(" ", "");
			String policyName = key + " - T.Con - " + sourceIp + " - " + destIp + " - " + policyService;
			String policyAliasFrom = "Source - " + sourceIp;
			String policyAliasTo = "Destination - " + destIp;
			String policyDescription = "Policy added on " + LocalDateTime.now().toString();
			
			Connection conn = null;
			String sql = "INSERT INTO dbo.nmt_firewall (name, description, service, reject_action, alias, alias2, proxy, nat, jira_ticket) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?);";
			
			try {
				
				conn = DB.getConnection();
				PreparedStatement stmt = conn.prepareStatement(sql);
				
				stmt.setString(1, policyName);
				stmt.setString(2, policyDescription);
				stmt.setString(3, policyService);
				stmt.setInt(4, 1);
				stmt.setString(5, policyAliasFrom);
				stmt.setString(6, policyAliasTo);
				stmt.setNull(7, Types.VARCHAR);
				stmt.setNull(8, Types.VARCHAR);
				stmt.setString(9, key);
				
				int success = stmt.executeUpdate();
				
				if (success > 0) {
					String body = "Diese Firewallregel wurde im IPNMT hinterlegt!";
					Email.sendMail(key, body);
				}
				
				stmt.close();
				
			} catch (Exception e) {
				e.printStackTrace();
			} finally {
				DB.closeConnection(conn);
			}
		
		}
		
	}

	/**
	 * @param json
	 * @throws MessagingException
	 * @throws JSONException
	 */
	@POST
	@Path("sendsupportmail")
	public void sendSupportMail(final String json) throws MessagingException, JSONException {
		
		JSONObject jsonObject = new JSONObject(json);
		JSONObject issue = (JSONObject) jsonObject.get("issue");
		JSONObject fields = (JSONObject) issue.get("fields");
		JSONObject reporter = (JSONObject) fields.get("reporter");
		String toAddress = reporter.getString("emailAddress");
		String username = reporter.getString("key");
		List<String> list = new ArrayList<String>();
		
		if (!"ext-buc-dv".equals(username) && !"ext-va-sapha".equals(username) && username.indexOf("helpdesk") < 0) {
		
			if (toAddress.indexOf("@team-con.de") < 0) {
	
				String sql = "SELECT * FROM dbo.supportumfrage WHERE datum = CONVERT(DATE, GETDATE())";
				Connection conn = null;
				
				try {
					
					conn = DB.getConnection();
					Statement stmt = conn.createStatement();
					ResultSet rs = stmt.executeQuery(sql);
					
					while(rs.next()) {
						
						list.add(rs.getString(2));
										
					}
					
				} catch (Exception e) {
					e.printStackTrace();
				} finally {
					DB.closeConnection(conn);
				}
				
				if(!list.contains(toAddress)) {
					
					sendHTMLMail(toAddress);
					
					String update = "INSERT INTO dbo.supportumfrage (email, datum) VALUES (?,?)";
					Connection conn2 = null;
					
					try {
						
						conn2 = DB.getConnection();
						PreparedStatement ps = conn2.prepareStatement(update);
						ps.setString(1, toAddress);
						ps.setDate(2, new java.sql.Date(new java.util.Date().getTime()));
						ps.executeUpdate();
						
						
					} catch (Exception e) {
						e.printStackTrace();
					} finally {
						DB.closeConnection(conn2);
					}
					
				}
			
			}
		
		}
	
	}
	
	@GET
	@Path("checkTicketFeedback")
	public String checkTicketFeedback() throws JSONException {
		
		StringBuilder bob = new StringBuilder();
		
		try {
			
			X509TrustManager x509TrustManager = new X509TrustManager() {
                public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                	return null;
                }
                public void checkClientTrusted(X509Certificate[] certs, String authType) {
                	//nix
                }
                public void checkServerTrusted(X509Certificate[] certs, String authType) {
                	//nix
                }
            };
            
            TrustManager[] trustAllCerts = new TrustManager[] {x509TrustManager};
	 
	        SSLContext sc = SSLContext.getInstance("SSL");
	        sc.init(null, trustAllCerts, new java.security.SecureRandom());
	        HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
	 
	        HostnameVerifier allHostsValid = new HostnameVerifier() {
	            public boolean verify(String hostname, SSLSession session) {
	                return true;
	            }
	        };
	 
	        HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);
	        
//			JSON Body
//			{
//			    "jql": "resolution = Unresolved AND status = AwaitingFeedback AND (project = tcitsup OR project = tcitint OR project = tcita OR project = tcitsnd) AND updated < startOfDay(-21d)",
//			    "startAt": 0,
//			    "maxResults": 1000,
//			    "fields": [
//			        "updated"
//			    ]
//			}
	        
	        JSONObject json = new JSONObject();
	        ArrayList<String> fieldList = new ArrayList<String>();
	        fieldList.add("updated");
	        String jqlQuery = "resolution = Unresolved AND status = AwaitingFeedback AND (project = tcitsup OR project = tcitint OR project = tcita OR project = tcitsnd) AND updated < startOfDay(-21d)";
	        
	        json.put("jql", jqlQuery);
	        json.put("startAt", 0);
	        json.put("maxResults", 1000);
	        json.put("fields", fieldList);
			
			String resturl = "https://jira.team-con.de/rest/api/2/search"; 
			
			URL url = new URL(resturl);
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setRequestMethod("POST");
			conn.setRequestProperty("Content-Type", "application/json");
			conn.setRequestProperty("Authorization", "Basic emVfdXNlcjp3N1JRcGw3YkQlLUpRdjMmIjloYg==");
			conn.setDoInput(true);
			conn.setDoOutput(true);
			
			OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
			wr.write(json.toString());
			wr.flush();
			
			int httpResult = conn.getResponseCode(); 
			
			if (httpResult == HttpURLConnection.HTTP_OK) {
			    
				BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream(), "utf-8"));
			    String line = null;
			    
			    while ((line = br.readLine()) != null) {  
			        bob.append(line + "\n");  
			    }
			    
			    br.close();
			    System.out.println("" + bob.toString());  
			    
			} else {
			    System.out.println(conn.getResponseMessage());  
			}  
			
			conn.disconnect();
			
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		} catch (KeyManagementException e) {
			e.printStackTrace();
		} catch (JSONException e) {
			e.printStackTrace();
		}
		
		JSONObject jsonObject = new JSONObject(bob.toString());
		int total = jsonObject.getInt("total");
		
		if (total > 0) {
		
			JSONArray issues = (JSONArray) jsonObject.get("issues");
			
			for (int i = 0; i < issues.length(); i++) {
				try {
					JSONObject issue = (JSONObject) issues.get(i);
					String key = issue.getString("key");
					doTransition(key, 791);
					addComment(key, "Dieses Ticket wurde aufgrund von Inaktivit&auml;t automatisch geschlossen.<br/>Sollten weiterhin Probleme bestehen, &ouml;ffne das Ticket bitte erneut.");
				} catch (Exception e) {
					return new Gson().toJson("Error: " + e.getMessage());
				}
			}
			
			return new Gson().toJson("Success");
		
		} else {
			return new Gson().toJson("No issues found!");
		}
		
	}
	
	private void sendHTMLMail(final String toAddress) throws MessagingException {

		String username = "tcon\\supportumfrage";
		String password = "fyt2RKBZGIqy0myi6Ji1";
		String fromAdress = "supportumfrage@team-con.de";
		String message = "<style type=\"text/css\">* [summary~=\"x-normal\"] a {color: #16bae7;text-decoration: underline !important;}* [summary~=\"x-normal\"] a:hover {color: #16bae7;text-decoration: underline !important;}* [summary~=\"x-item-normal\"] a {color: #15bae6;text-decoration: underline !important;}* [summary~=\"x-item-normal\"] a:hover {color: #15bae6;text-decoration: underline !important;}a, * [summary~=\"x-item-highlighted\"] a {color: #f59a00;text-decoration: underline !important;}a:hover, * [summary~=\"x-item-highlighted\"] a:hover {color: #f59a00;text-decoration: underline !important;}.highlighted a {color:#f59a00 !important;}</style><style type=\"text/css\">/*Default LAYOUT*/ td {font-size: 12px;font-family: Arial, Helvetica Neue, Helvetica, sans-serif;color: #424242;}#leadout_signature td{color: #999999;}a {color: #16bae7;text-decoration: underline !important;}a:hover {color: #16bae7;text-decoration: underline !important;}/*IOS BUGFIX*/a[x-apple-data-detectors] {color: inherit !important;text-decoration: none !important;font-size: inherit !important;font-family: inherit !important;font-weight: inherit !important;line-height: inherit !important;}@media (max-width:620px) {table {width: 100% !important;}[class*=\"formWidth\"] {width: 100%;}html td[class=\"ivIcon\"] {width: 7% !important;}html td[class=\"ivText\"] {width: 93% !important;}[class*=\"stretchImage\"] img {width: 100% !important;height: auto !important;}img [class*=\"stretchWidth\"] {width: 100% !important;}[class*=\"paddingOutside\"] {padding-left: 10px;padding-right: 10px;}[class*=\"paddingInside\"] {padding-left: 10px;padding-right: 10px;}[class*=\"hide620\"] {display: none;}[class*=\"topLeft\"] {padding-left: 10px;}[class*=\"topRight\"] {padding-right: 10px;}[class*=\"nl_button\"] td {padding: 10px 3px;}}@media (max-width:600px) {[class*=\"breakTable500\"],[class*=\"breakTable500\"] tbody,[class*=\"breakTable500\"] tr,[class*=\"breakTable500\"] td, [class*=\"breakCell500\"] {float: left !important;width: 100% !important;}[class*=\"stopBreak500\"],[class*=\"stopBreak500\"] tbody,[class*=\"stopBreak500\"] tr,[class*=\"stopBreak500\"] td {float: none !important;width: 100% !important;}[class*=\"no_break\"],[class*=\"no_break\"] tbody,[class*=\"no_break\"] tr,[class*=\"no_break\"] td {float: none !important;width: auto !important;}[class*=\"paddingPic600\"] {padding-bottom: 10px;}}@media (max-width:500px) {[class*=\"breakTable500\"],[class*=\"breakTable500\"] tbody,[class*=\"breakTable500\"] tr,[class*=\"breakTable500\"] td, [class*=\"breakCell500\"] {float: left;width: 100%;}[class*=\"breakTable500\"] [class*=\"pictureStretch350\"] img {margin: 0 auto;}[class*=\"hide500\"] {display: none;}[class*=\"displayBlock500\"] {display: block;}[class*=\"paddingContent500\"] {padding-top: 10px;}[class*=\"paddingItem500\"] {padding-top: 15px;padding-bottom: 15px;}[class*=\"paddingInside500\"] {padding-left: 10px;padding-right: 10px;}[class*=\"paddingBottom500\"] {padding-bottom: 15px;}td[class=\"ivIcon\"] {width: 7% !important;}td[class=\"ivText\"] {width: 93% !important;}}@media (max-width:350px) {[class*=\"pictureStretch350\"] img {max-width: 100% !important;height: auto !important;}}@media (max-width:230px) {[class*=\"picStretch230\"] img {max-width: 100% !important;height: auto !important;}}</style><style type=\"text/css\"> /*LINK Leadin*/#leadin a, .leadin a{color: #16bae7 !important;}/*LINK Content*/#contentelement a, .contentelement a{color: #15bae6 !important;}/*LINK TOC*/.toc a, #toc a {color:#5c6970 !important;text-decoration: none !important;}.toc a:hover, #toc a {color:#5c6970 !important;text-decoration: !important;}/*highlighted*/#contentelement .highlighted a {color:#f59a00 !important;}/*LINK Leadout*/.leadout_signature a, #leadout_signature a {color: #999999 !important;}.leadout_text a, #leadout_test a {color: #999999 !important;}/*LINK Buttons*/.buttons a, #buttons a {text-decoration: none;color: #999999;}#contentelement #nl_buttonnormal a{color: #ffffff !important;}#contentelement #nl_buttonhighlighted a{color: #ffffff !important;}</style><a name=\"contents\"></a><div style=\"background-color:#e9f2f4;\"><table name=\"top\" height=\"100%\" width=\"100%\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\" id=\"top\"><tr><td width=\"100%\" valign=\"top\" align=\"center\" ><table width=\"620\" class=\"paddingOutside\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\" align=\"center\"><tr><td width=\"620\"><table width=\"100%\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\"><tr><td width=\"100%\" bgcolor=\"#ffffff\" align=\"center\"><p>Falls diese E-Mail nicht korrekt dargestellt werden sollte, verwenden Sie bitte <a href=\"https://cdn.mlwrx.com/sys/w.aspx?sub=GF7_00000&t=t&cmp=56a00a851f21c5a057d29f8980075fc0\">diesen Link</a></p></td></tr><tr><td width=\"100%\" class=\"stretchImage\"  bgcolor=\"#ffffff\"><a href=\"https://cdn.mlwrx.com/sys/r.aspx?sub=GF7_00000&amp;t=t&link=cF2c\"><img src=\"https://cdn.mlwrx.com/Media/a5fe1618-149f-4072-9943-f6e3feb6415f/2017/Kundenumfrage---Allgemein_2017/_headerbild_v41_9a1c13e9-7f0b-4981-9d2f-61ca53b0d18d.png\" border=\"0\" style=\"display:block;\" width=\"620\" height=\"272\" alt=\"Headerbild v41\"></a></td></tr><tr><td width=\"100%\"  bgcolor=\"#ffffff\" height=\"10\" style=\"line-height:10px;font-size:10px;\">&nbsp;</td></tr><tr><td width=\"620\" id=\"leadin\" class=\"leadin\" class=\"paddingInside\" align=\"center\"  bgcolor=\"#ffffff\"><table width=\"94%\" border=\"0\" class=\"breakTable500\" cellpadding=\"0\" cellspacing=\"0\" align=\"center\"><tr><td width=\"2%\" class=\"hide620\" valign=\"top\" bgcolor=\"#ffffff\">&nbsp;</td><td summary=\"x-normal\" bgcolor=\"#ffffff\" width=\"98%\" valign=\"top\"><table width=\"97%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\"><tr><td width=\"100%\" height=\"10\" bgcolor=\"#ffffff\" style=\"line-height:10px;font-size:10px;\">&nbsp;</td></tr><tr><td width=\"100%\"  bgcolor=\"#ffffff\" valign=\"top\" align=\"left\"><div class=\"paddingInside500\"><span style=\"color:#afc0c4;font-weight:bold; font-size: 22px;font-family:Arial, Helvetica Neue, Helvetica, sans-serif;\">Waren Sie mit dem Support zufrieden?</span></div></td></tr><tr><td width=\"100%\" height=\"10\" style=\"line-height:10px;font-size:10px;\" bgcolor=\"#ffffff\">&nbsp;</td></tr><tr><td width=\"100%\" bgcolor=\"#ffffff\" valign=\"top\" align=\"left\"><div class=\"paddingInside500\"><span style=\"font-size: 12px;font-family:Arial, Helvetica Neue, Helvetica, sans-serif; color:#424242;\"><div style=\"text-align: justify;\">Wir m&ouml;chten herausfinden, wie &uuml;berzeugt und zufrieden Sie mit unserem Support&nbsp;sind. Deshalb bitten wir Sie, sich 1&nbsp;Minute Zeit zu nehmen und an unserer kurzen Befragung teilzunehmen.<br />F&uuml;r Ihre Teilnahme m&ouml;chten wir uns im Voraus ganz herzlich bei Ihnen bedanken.</div></span></div></td></tr><tr><td width=\"100%\" height=\"10\" bgcolor=\"#ffffff\" style=\"line-height:10px;font-size:10px;\">&nbsp;</td></tr></table></td><td width=\"1%\" class=\"hide620\" valign=\"top\" bgcolor=\"#ffffff\">&nbsp;</td></tr></table></td></tr><tr><td width=\"100%\" height=\"10\"  bgcolor=\"#ffffff\" style=\"line-height:10px;font-size:10px;\">&nbsp;</td></tr><tr><td id=\"contentelement\" width=\"620\" class=\"contentelement stretchImage\" align=\"center\"  bgcolor=\"#ffffff\"><table width=\"94%\" class=\"paddingInside\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\" align=\"center\"><tr><td width=\"100%\" height=\"10\" style=\"line-height:10px;font-size:10px;\" bgcolor=\"#ffffff\">&nbsp;</td></tr><tr><td width=\"100%\" bgcolor=\"#ffffff\"><table width=\"96%%\" class=\"paddingInside\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\" align=\"center\"><tr><td width=\"100%\" summary=\"x-item-normal\" class=\"\" align=\"left\" bgcolor=\"#ffffff\"><span class=\"contentelement \" style=\"font-size:12px;font-family:Arial, Helvetica Neue, Helvetica, sans-serif;color:#424242;\"></span></td></tr><tr><td><table align=\"center\" class=\"nl_button no_break\" id=\"nl_buttonnormal\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\"><tr><td width=\"10\" bgcolor=\"#16bae7\"  style=\"line-height: 10px; font-size: 10px;\">&nbsp;</td><td height=\"25\" bgcolor=\"#16bae7\" style=\"height:25px; vertical-align: middle;\"><a href=\"https://cdn.mlwrx.com/sys/r.aspx?sub=GF7_00000&amp;t=t&link=cF2d\" style=\"text-decoration: none;font-family:Arial, Helvetica Neue, Helvetica, sans-serif;color:#FFFFFF !important;font-size:16px;line-height:16px;\"><span style=\"color:#FFFFFF !important;\">zur Umfrage</span></a></td><td width=\"10\" bgcolor=\"#16bae7\"  style=\"line-height: 10px; font-size: 10px;\">&nbsp;</td></tr><tr><td width=\"10\"  style=\"line-height: 10px; font-size: 10px;\">&nbsp;</td></tr></table></td></tr></table></td></tr></table></td></tr><tr><td width=\"620\" height=\"10\" style=\"line-height:10px;font-size:10px;\"  bgcolor=\"#ffffff\">&nbsp;</td></tr><tr><td width=\"100%\" height=\"20\"  bgcolor=\"#ffffff\" style=\"line-height:10px;font-size:10px;\"><a name=\"sec_782297b8526d41a1a72e829f34d03241\"><span></span></a>&nbsp;</td></tr><!-- /articlelarge --><!-- /mx_sec_articlelarge --><!-- mx_sec_article --><!-- article --><tr><td id=\"contentelement\" class=\"contentelement\" width=\"620\"  bgcolor=\"#ffffff\" align=\"center\"><a name=\"sec_d3644a06028c42daa944c377404040a8\"><span></span></a><table width=\"94%\" class=\"paddingInside breakTable500 \" summary=\"x-item-normal\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" align=\"center\"><tr><td width=\"100%\" valign=\"top\" bgcolor=\"#ffffff\"><table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\"><tr><td width=\"100%\" bgcolor=\"#ffffff\" style=\"border-top:5px solid #ffffff; border-bottom:5px solid #ffffff;\"><table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\"><tr><td class=\"hide620\" bgcolor=\"#ffffff\" width=\"2%\">&nbsp;</td><td width=\"96%\" class=\"stopBreak500 \" bgcolor=\"#ffffff\" valign=\"top\" align=\"left\"><div class=\"paddingInside\"><span class=\"contentelement \" style=\"font-size:12px;font-family:Arial, Helvetica Neue, Helvetica, sans-serif;color:#424242;\"><!-- <span class=\"contentelement\" style=\"font-weight:bold; font-size:17px; color:#16bae7;\" size=\"4\"> --><hr /><br />How satisfied are you with the support in general? Please help us improve our service in the future and answer a few questions. We very much appreciate your comments. <a style=\"color:#15bae6;\" href=\"https://cdn.mlwrx.com/sys/r.aspx?sub=GF7_00000&amp;t=t&amp;link=cRa0\">Click here for the Survey</a>.<br />&nbsp;</span></div></td><td class=\"hide620\" bgcolor=\"#ffffff\" width=\"2%\">&nbsp;</td></tr></table></td></tr><tr><td width=\"100%\" bgcolor=\"#ffffff\"></td></tr><tr><td width=\"100%\" align=\"center\" bgcolor=\"#ffffff\" valign=\"top\" align=\"left\"><table width=\"96%\" class=\"paddingInside\" cellpadding=\"0\" cellspacing=\"0\" boder=\"0\" align=\"center\"><tr><td width=\"100%\" class=\"\" bgcolor=\"#ffffff\" valign=\"top\" align=\"left\"><span class=\"contentelement \" style=\"font-size:12px;font-family:Arial, Helvetica Neue, Helvetica, sans-serif;color:#424242;\">Wir w&uuml;nschen Ihnen alles Gute und freuen uns auf eine weiterhin erfolgreiche Zusammenarbeit!<br /><br />Herzliche Gr&uuml;&szlig;e<br /><br /><span style=\"font-size:14px;\"><strong>Norbert Kytka</strong></span><br /><span style=\"font-size:11px;\">Director Sales and Marketing</span></span></td></tr><tr><td width=\"100%\" height=\"10\" style=\"line-height:10px;font-size:10px;\" bgcolor=\"#ffffff\">&nbsp;</td></tr></table></td></tr></table></td></tr></table></td></tr><tr><td width=\"100%\" height=\"20\" bgcolor=\"#ffffff\" style=\"line-height:10px;font-size:10px;height:20px;\">&nbsp;</td></tr><tr><td width=\"100%\" bgcolor=\"#ffffff\" align=\"center\"><table width=\"91%\" class=\"paddingInside breakTable500\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\"><tr><td width=\"100%\" height=\"10\" bgcolor=\"#ffffff\" style=\"line-height:10px;font-size:10px;\">&nbsp;</td></tr><tr><td width=\"100%\" height=\"14\" bgcolor=\"#ffffff\" style=\"line-height:10px;font-size:10px;\">&nbsp;</td></tr></table></td></tr></table></td></tr><tr><td width=\"620\" height=\"10\" style=\"line-height: 10px; font-size: 10px;\">&nbsp;</td></tr><tr><td width=\"620\" align=\"center\"><table width=\"91%\" class=\"paddingInside breakTable500\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\"><tr><td width=\"100%\" align=\"left\"><table style=\"width:100%;\" align=\"\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\"><tr><td><span id=\"leadout_text\" class=\"leadout_text\" style=\"font-size:11px;font-family:Arial, Helvetica Neue, Helvetica, sans-serif;color:#999999\"><br /><span style=\"font-size:11px;\"><span style=\"font-family: arial,helvetica,sans-serif;\"><strong><span style=\"font-size:12px;\">T.CON GmbH &amp; Co. KG</span></strong></span></span><br /><br /><span style=\"font-size:10px;\"><span style=\"font-family: arial,helvetica,sans-serif;\">Hauptsitz<br />Straubinger Stra&szlig;e 2<br />94447 Plattling<br />T: +49 (0)9931 981 100<br />F: +49 (0)9931 981 199</span></span><br /><br /><img alt=\"SAP GoldPartner Logo\" border=\"0\" height=\"40\" src=\"https://cdn.mlwrx.com/Media/a5fe1618-149f-4072-9943-f6e3feb6415f/sap_goldpartner_grad_r.png\" style=\"border-width: 0px;\" width=\"69\" /><br /><br /><span style=\"font-size:9px;\"><span style=\"font-family: arial,helvetica,sans-serif;\">Gesch&auml;ftsf&uuml;hrer Karl Fuchs, Michael Gulde, Stefan Fiedler | KG AG Deggendorf HRA 1618 | Pers. haftender Ges. T.CON Beteiligungs-GmbH | AG Deggendorf HRB 2053<br /><br />youtube | xing&nbsp;| linkedin</span></span><br /><br />&nbsp;</span></td></tr></table></td></tr></table></td></tr><tr><td width=\"620\" height=\"10\" style=\"line-height: 10px; font-size: 10px;\">&nbsp;</td></tr></table><div style=\"clear: both;\"></div>&nbsp;</td></tr></table></div>";
		String subject = "Waren Sie mit dem Support zufrieden?";
		
        Properties properties = new Properties();
        properties.put("mail.smtp.host", "mail.tcon-group.de");
        properties.put("mail.smtp.port", "25");
        properties.put("mail.smtp.starttls.enable", "true");
        properties.put("mail.smtp.auth", "true");
 
        Session session = Session.getInstance(properties,
			new javax.mail.Authenticator() {
				protected PasswordAuthentication getPasswordAuthentication() {
					return new PasswordAuthentication(username, password);
				}
       		}
        );
 
        Message msg = new MimeMessage(session);
 
        msg.setFrom(new InternetAddress(fromAdress));
        InternetAddress[] toAddresses = { new InternetAddress(toAddress) };
        msg.setRecipients(Message.RecipientType.TO, toAddresses);
        msg.setSubject(subject);
        msg.setSentDate(new Date());
        msg.setContent(message, "text/html");
 
        Transport.send(msg);
		
	}
	
	private String isProjectTimeTrackingRelevant(String projectkey) throws JSONException {
		
		Boolean found = false;
		String res = "true";
		String projectdesc = null;
		StringBuilder bob = new StringBuilder();
		
		try {
		
	        X509TrustManager x509TrustManager = new X509TrustManager() {
                public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                	return null;
                }
                public void checkClientTrusted(X509Certificate[] certs, String authType) {
                	//nix
                }
                public void checkServerTrusted(X509Certificate[] certs, String authType) {
                	//nix
                }
            };
            
            TrustManager[] trustAllCerts = new TrustManager[] {x509TrustManager};
	 
	        SSLContext sc = SSLContext.getInstance("SSL");
	        sc.init(null, trustAllCerts, new java.security.SecureRandom());
	        HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
	 
	        HostnameVerifier allHostsValid = new HostnameVerifier() {
	            public boolean verify(String hostname, SSLSession session) {
	                return true;
	            }
	        };
	 
	        HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);
			
			String resturl = "https://jira.team-con.de/rest/api/2/project/" + projectkey;
			
			URL url = new URL(resturl);
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setRequestMethod("GET");
			conn.setRequestProperty("Content-Type", "application/json");
			conn.setRequestProperty("Authorization", "Basic emVfdXNlcjp3N1JRcGw3YkQlLUpRdjMmIjloYg==");
			
			if (conn.getResponseCode() != 200) {
				throw new RuntimeException("Failed: HTTP error code: " + conn.getResponseCode());
			}
			
			BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"));
			
			String output;
			while ((output = br.readLine()) != null) {
				bob.append(output);
			}
			
			conn.disconnect();
		
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		} catch (KeyManagementException e) {
			e.printStackTrace();
		}
		
		JSONObject jsonObject = new JSONObject(bob.toString());
		projectdesc = jsonObject.getString("description");
		
		
		if(projectdesc != null) {
			
			String[] split = projectdesc.split(";");
			
			for(int i = 0; i < split.length; i++) {
				
				if(split[i].indexOf("FlagZeiterfassung") > -1) {
					found = true;
					
					String test = split[i].split("\\[")[1].replace("\\]", "");
					
					if(test.equals("false")) {
						res = "false";
					}
				} else {
					res = "false";
				}
				
			}
			
		}
		
		if(!found) {
			res = "nicht vorhanden";
		}
		
		return res;
		
	}
	
	private String isIssueTimeTrackingRelevant(String issuekey) throws JSONException {
		String res = "false";
		String value = null;
		StringBuilder bob = new StringBuilder();
		
		try {
			
			X509TrustManager x509TrustManager = new X509TrustManager() {
                public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                	return null;
                }
                public void checkClientTrusted(X509Certificate[] certs, String authType) {
                	//nix
                }
                public void checkServerTrusted(X509Certificate[] certs, String authType) {
                	//nix
                }
            };
            
            TrustManager[] trustAllCerts = new TrustManager[] {x509TrustManager};
	 
	        SSLContext sc = SSLContext.getInstance("SSL");
	        sc.init(null, trustAllCerts, new java.security.SecureRandom());
	        HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
	 
	        HostnameVerifier allHostsValid = new HostnameVerifier() {
	            public boolean verify(String hostname, SSLSession session) {
	                return true;
	            }
	        };
	 
	        HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);
			
			String resturl = "https://jira.team-con.de/rest/api/2/issue/" + issuekey;
			
			URL url = new URL(resturl);
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setRequestMethod("GET");
			conn.setRequestProperty("Content-Type", "application/json");
			conn.setRequestProperty("Authorization", "Basic emVfdXNlcjp3N1JRcGw3YkQlLUpRdjMmIjloYg==");
			
			if (conn.getResponseCode() != 200) {
				throw new RuntimeException("Failed: HTTP error code: " + conn.getResponseCode());
			}
			
			BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"));
			
			String output;
			while ((output = br.readLine()) != null) {
				bob.append(output);
			}
			
			conn.disconnect();
		
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		} catch (KeyManagementException e) {
			e.printStackTrace();
		}
		
		JSONObject jsonObject = new JSONObject(bob.toString());
		JSONObject fields = (JSONObject) jsonObject.get("fields");
		
		try {
			JSONObject custfield = (JSONObject) fields.get("customfield_13506");
			value = custfield.getString("value");
		} catch (Exception e) {
			value = "nicht vorhanden";
			res = "nicht vorhanden";
		}
		
		if(value != null) {
			
			if(value.equals("Ja")) {
				res = "true";
			}
			
		}
		
		return res;
	}
	
	private String getParentIssue(final String key) {
		
		String parentKey = "";
		
		StringBuilder bob = new StringBuilder();
		
		try {
			
			X509TrustManager x509TrustManager = new X509TrustManager() {
                public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                	return null;
                }
                public void checkClientTrusted(X509Certificate[] certs, String authType) {
                	//nix
                }
                public void checkServerTrusted(X509Certificate[] certs, String authType) {
                	//nix
                }
            };
            
            TrustManager[] trustAllCerts = new TrustManager[] {x509TrustManager};
	 
	        SSLContext sc = SSLContext.getInstance("SSL");
	        sc.init(null, trustAllCerts, new java.security.SecureRandom());
	        HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
	 
	        HostnameVerifier allHostsValid = new HostnameVerifier() {
	            public boolean verify(String hostname, SSLSession session) {
	                return true;
	            }
	        };

	        HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);
			
			String resturl = "https://jira.team-con.de/rest/api/2/issue/" + key;
			
			URL url = new URL(resturl);
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setRequestMethod("GET");
			conn.setRequestProperty("Content-Type", "application/json");
			conn.setRequestProperty("Authorization", "Basic emVfdXNlcjp3N1JRcGw3YkQlLUpRdjMmIjloYg==");
			
			if (conn.getResponseCode() != 200) {
				throw new RuntimeException("Failed: HTTP error code: " + conn.getResponseCode());
			}
			
			BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"));
			
			String output;
			while ((output = br.readLine()) != null) {
				bob.append(output);
			}
			
			conn.disconnect();
		
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		} catch (KeyManagementException e) {
			e.printStackTrace();
		}
		
		JSONObject jsonObject;
		try {
			jsonObject = new JSONObject(bob.toString());
			JSONObject fields = (JSONObject) jsonObject.get("fields");
			JSONObject parent = (JSONObject) fields.get("parent");
			parentKey = parent.getString("key");
		} catch (JSONException e) {
			e.printStackTrace();
		}
		
		return parentKey;
		
	}
	
	private Boolean areSubtasksClosed(final String parentKey) {
		
		Boolean subtasksClosed = true;
		
		StringBuilder bob = new StringBuilder();
		
		try {
			
			X509TrustManager x509TrustManager = new X509TrustManager() {
                public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                	return null;
                }
                public void checkClientTrusted(X509Certificate[] certs, String authType) {
                	//nix
                }
                public void checkServerTrusted(X509Certificate[] certs, String authType) {
                	//nix
                }
            };
            
            TrustManager[] trustAllCerts = new TrustManager[] {x509TrustManager};
	 
	        SSLContext sc = SSLContext.getInstance("SSL");
	        sc.init(null, trustAllCerts, new java.security.SecureRandom());
	        HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
	 
	        HostnameVerifier allHostsValid = new HostnameVerifier() {
	            public boolean verify(String hostname, SSLSession session) {
	                return true;
	            }
	        };
	 
	        HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);
			
			String resturl = "https://jira.team-con.de/rest/api/2/issue/" + parentKey + "/subtask"; 
			
			URL url = new URL(resturl);
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setRequestMethod("GET");
			conn.setRequestProperty("Content-Type", "application/json");
			conn.setRequestProperty("Authorization", "Basic emVfdXNlcjp3N1JRcGw3YkQlLUpRdjMmIjloYg==");
			
			if (conn.getResponseCode() != 200) {
				throw new RuntimeException("Failed: HTTP error code: " + conn.getResponseCode());
			}
			
			BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"));
			
			String output;
			while ((output = br.readLine()) != null) {
				bob.append(output);
			}
			
			conn.disconnect();
		
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		} catch (KeyManagementException e) {
			e.printStackTrace();
		}
		
		try {
			
			JSONArray subtasks = new JSONArray(bob.toString());
			
			for (int i = 0; i < subtasks.length(); i++) {
				
				JSONObject subtask = (JSONObject) subtasks.get(i);
				JSONObject fields = (JSONObject) subtask.get("fields");
				JSONObject status = (JSONObject) fields.get("status");
				int statusId = status.getInt("id");
				
				if (statusId != 6) {
					subtasksClosed = false;
					break;
				}
				
			}
			
		} catch (JSONException e) {
			e.printStackTrace();
		}
		
		return subtasksClosed;
	}
	
	/**
	 * @param key
	 * @param transitionId
	 */
	private void doTransition(final String key, final int transitionId) {
		
		try {
			
			X509TrustManager x509TrustManager = new X509TrustManager() {
                public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                	return null;
                }
                public void checkClientTrusted(X509Certificate[] certs, String authType) {
                	//nix
                }
                public void checkServerTrusted(X509Certificate[] certs, String authType) {
                	//nix
                }
            };
            
            TrustManager[] trustAllCerts = new TrustManager[] {x509TrustManager};
	 
	        SSLContext sc = SSLContext.getInstance("SSL");
	        sc.init(null, trustAllCerts, new java.security.SecureRandom());
	        HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
	 
	        HostnameVerifier allHostsValid = new HostnameVerifier() {
	            public boolean verify(String hostname, SSLSession session) {
	                return true;
	            }
	        };
	 
	        HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);
	        
	        // JSON body
	        //
	        //{
	        //  "transition" : {
	        //		"id" : "41"
	        //	}
	        //}
	        
	        JSONObject json = new JSONObject();
	        JSONObject id = new JSONObject();
	        
	        id.put("id", "" + transitionId);
	        json.put("transition", id);
			
			String resturl = "https://jira.team-con.de/rest/api/latest/issue/" + key + "/transitions"; 
			
			URL url = new URL(resturl);
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setRequestMethod("POST");
			conn.setRequestProperty("Content-Type", "application/json");
			conn.setRequestProperty("Authorization", "Basic emVfdXNlcjp3N1JRcGw3YkQlLUpRdjMmIjloYg==");
			conn.setDoInput(true);
			conn.setDoOutput(true);
			
			OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
			wr.write(json.toString());
			wr.flush();
			
			StringBuilder bob = new StringBuilder();  
			int httpResult = conn.getResponseCode(); 
			
			if (httpResult == HttpURLConnection.HTTP_OK) {
			    
				BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream(), "utf-8"));
			    String line = null;
			    
			    while ((line = br.readLine()) != null) {  
			        bob.append(line + "\n");  
			    }
			    
			    br.close();
			    System.out.println("" + bob.toString());  
			    
			} else {
			    System.out.println(conn.getResponseMessage());  
			}  
			
			conn.disconnect();
		
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		} catch (KeyManagementException e) {
			e.printStackTrace();
		} catch (JSONException e) {
			e.printStackTrace();
		}
		
	}
	
	private Boolean isParent(final String key) {
		
		Boolean isParent = false;
		
		StringBuilder bob = new StringBuilder();
		
		try {
			
			X509TrustManager x509TrustManager = new X509TrustManager() {
                public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                	return null;
                }
                public void checkClientTrusted(X509Certificate[] certs, String authType) {
                	//nix
                }
                public void checkServerTrusted(X509Certificate[] certs, String authType) {
                	//nix
                }
            };
            
            TrustManager[] trustAllCerts = new TrustManager[] {x509TrustManager};
	 
	        SSLContext sc = SSLContext.getInstance("SSL");
	        sc.init(null, trustAllCerts, new java.security.SecureRandom());
	        HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
	 
	        HostnameVerifier allHostsValid = new HostnameVerifier() {
	            public boolean verify(String hostname, SSLSession session) {
	                return true;
	            }
	        };

	        HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);
			
			String resturl = "https://jira.team-con.de/rest/api/2/issue/" + key;
			
			URL url = new URL(resturl);
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setRequestMethod("GET");
			conn.setRequestProperty("Content-Type", "application/json");
			conn.setRequestProperty("Authorization", "Basic emVfdXNlcjp3N1JRcGw3YkQlLUpRdjMmIjloYg==");
			
			if (conn.getResponseCode() != 200) {
				throw new RuntimeException("Failed: HTTP error code: " + conn.getResponseCode());
			}
			
			BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"));
			
			String output;
			while ((output = br.readLine()) != null) {
				bob.append(output);
			}
			
			conn.disconnect();
		
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		} catch (KeyManagementException e) {
			e.printStackTrace();
		}
		
		JSONObject jsonObject;
		try {
			jsonObject = new JSONObject(bob.toString());
			JSONObject fields = (JSONObject) jsonObject.get("fields");
			JSONArray subtasks = (JSONArray) fields.get("subtasks");

			if (subtasks.length() > 0) {
				isParent = true;
			}
			
		} catch (JSONException e) {
			e.printStackTrace();
		}
		
		return isParent;
		
	}
	
	private HashMap<String, String> getLastComment(final String key) {
		
		HashMap<String, String> map = new HashMap<String, String>();
		
		StringBuilder bob = new StringBuilder();
		
		try {
			
			X509TrustManager x509TrustManager = new X509TrustManager() {
                public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                	return null;
                }
                public void checkClientTrusted(X509Certificate[] certs, String authType) {
                	//nix
                }
                public void checkServerTrusted(X509Certificate[] certs, String authType) {
                	//nix
                }
            };
            
            TrustManager[] trustAllCerts = new TrustManager[] {x509TrustManager};
	 
	        SSLContext sc = SSLContext.getInstance("SSL");
	        sc.init(null, trustAllCerts, new java.security.SecureRandom());
	        HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
	 
	        HostnameVerifier allHostsValid = new HostnameVerifier() {
	            public boolean verify(String hostname, SSLSession session) {
	                return true;
	            }
	        };

	        HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);
			
			String resturl = "https://jira.team-con.de/rest/api/2/issue/" + key + "/comment";
			
			URL url = new URL(resturl);
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setRequestMethod("GET");
			conn.setRequestProperty("Content-Type", "application/json");
			conn.setRequestProperty("Authorization", "Basic emVfdXNlcjp3N1JRcGw3YkQlLUpRdjMmIjloYg==");
			
			if (conn.getResponseCode() != 200) {
				throw new RuntimeException("Failed: HTTP error code: " + conn.getResponseCode());
			}
			
			BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"));
			
			String output;
			while ((output = br.readLine()) != null) {
				bob.append(output);
			}
			
			conn.disconnect();
		
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		} catch (KeyManagementException e) {
			e.printStackTrace();
		}
		
		JSONObject jsonObject;
		try {
			jsonObject = new JSONObject(bob.toString());
			JSONArray comments = (JSONArray) jsonObject.get("comments");
			
			JSONObject lastComment = (JSONObject) comments.get(comments.length() - 1);
			map.put("body", lastComment.getString("body"));
			JSONObject author = (JSONObject) lastComment.get("author");
			map.put("authorName", author.getString("displayName"));
			
		} catch (JSONException e) {
			e.printStackTrace();
		}
		
		return map;
		
	}
	
	private void addComment(final String key, final String body) {
		
		try {
			
			X509TrustManager x509TrustManager = new X509TrustManager() {
                public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                	return null;
                }
                public void checkClientTrusted(X509Certificate[] certs, String authType) {
                	//nix
                }
                public void checkServerTrusted(X509Certificate[] certs, String authType) {
                	//nix
                }
            };
            
            TrustManager[] trustAllCerts = new TrustManager[] {x509TrustManager};
	 
	        SSLContext sc = SSLContext.getInstance("SSL");
	        sc.init(null, trustAllCerts, new java.security.SecureRandom());
	        HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
	 
	        HostnameVerifier allHostsValid = new HostnameVerifier() {
	            public boolean verify(String hostname, SSLSession session) {
	                return true;
	            }
	        };
	 
	        HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);
	        
	        // JSON body
	        //
	        //{
	        //	  "body" : "Wer das liest, ist doof"
	        //}
	        
	        JSONObject json = new JSONObject();
	        json.put("body", body);
			
			String resturl = "https://jira.team-con.de/rest/api/latest/issue/" + key + "/comment"; 
			
			URL url = new URL(resturl);
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setRequestMethod("POST");
			conn.setRequestProperty("Content-Type", "application/json");
			conn.setRequestProperty("Authorization", "Basic S29tbWVudGFyc3luYzpLb21tZW50QHIwMSE=");
			conn.setDoInput(true);
			conn.setDoOutput(true);
			
			OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
			wr.write(json.toString());
			wr.flush();
			
			StringBuilder bob = new StringBuilder();  
			int httpResult = conn.getResponseCode(); 
			
			if (httpResult == HttpURLConnection.HTTP_OK || httpResult == HttpsURLConnection.HTTP_CREATED) {
			    
				BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream(), "utf-8"));
			    String line = null;
			    
			    while ((line = br.readLine()) != null) {  
			        bob.append(line + "\n");  
			    }
			    
			    br.close();
			    System.out.println("" + bob.toString());  
			    
			} else {
			    System.out.println(conn.getResponseMessage());  
			}  
			
			conn.disconnect();
		
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		} catch (KeyManagementException e) {
			e.printStackTrace();
		} catch (JSONException e) {
			e.printStackTrace();
		}
		
	}
	
	private ArrayList<String> getSubtaskKeys(final String key) {
		
		ArrayList<String> list = new ArrayList<String>();
		
		StringBuilder bob = new StringBuilder();
		
		try {
			
			X509TrustManager x509TrustManager = new X509TrustManager() {
                public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                	return null;
                }
                public void checkClientTrusted(X509Certificate[] certs, String authType) {
                	//nix
                }
                public void checkServerTrusted(X509Certificate[] certs, String authType) {
                	//nix
                }
            };
            
            TrustManager[] trustAllCerts = new TrustManager[] {x509TrustManager};
	 
	        SSLContext sc = SSLContext.getInstance("SSL");
	        sc.init(null, trustAllCerts, new java.security.SecureRandom());
	        HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
	 
	        HostnameVerifier allHostsValid = new HostnameVerifier() {
	            public boolean verify(String hostname, SSLSession session) {
	                return true;
	            }
	        };
	 
	        HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);
			
			String resturl = "https://jira.team-con.de/rest/api/2/issue/" + key + "/subtask"; 
			
			URL url = new URL(resturl);
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setRequestMethod("GET");
			conn.setRequestProperty("Content-Type", "application/json");
			conn.setRequestProperty("Authorization", "Basic emVfdXNlcjp3N1JRcGw3YkQlLUpRdjMmIjloYg==");
			
			if (conn.getResponseCode() != 200) {
				throw new RuntimeException("Failed: HTTP error code: " + conn.getResponseCode());
			}
			
			BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"));
			
			String output;
			while ((output = br.readLine()) != null) {
				bob.append(output);
			}
			
			conn.disconnect();
		
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		} catch (KeyManagementException e) {
			e.printStackTrace();
		}
		
		try {
			
			JSONArray subtasks = new JSONArray(bob.toString());
			
			for (int i = 0; i < subtasks.length(); i++) {
				
				JSONObject subtask = (JSONObject) subtasks.get(i);
				
				String subtaskKey = subtask.getString("key");
				list.add(subtaskKey);
				
			}
			
		} catch (JSONException e) {
			e.printStackTrace();
		}
		
		return list;
		
	}
	
	private JSONObject getTicketInfo(final String key) throws JSONException {
		
		StringBuilder bob = new StringBuilder();
		
		try {
		
	        X509TrustManager x509TrustManager = new X509TrustManager() {
                public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                	return null;
                }
                public void checkClientTrusted(X509Certificate[] certs, String authType) {
                	//nix
                }
                public void checkServerTrusted(X509Certificate[] certs, String authType) {
                	//nix
                }
            };
            
            TrustManager[] trustAllCerts = new TrustManager[] {x509TrustManager};
	 
	        SSLContext sc = SSLContext.getInstance("SSL");
	        sc.init(null, trustAllCerts, new java.security.SecureRandom());
	        HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
	 
	        HostnameVerifier allHostsValid = new HostnameVerifier() {
	            public boolean verify(String hostname, SSLSession session) {
	                return true;
	            }
	        };
	 
	        HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);
			
			String resturl = "https://jira.team-con.de/rest/api/2/issue/" + key;
			
			URL url = new URL(resturl);
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setRequestMethod("GET");
			conn.setRequestProperty("Content-Type", "application/json");
			conn.setRequestProperty("Authorization", "Basic emVfdXNlcjp3N1JRcGw3YkQlLUpRdjMmIjloYg==");
			
			if (conn.getResponseCode() != 200) {
				throw new RuntimeException("Failed: HTTP error code: " + conn.getResponseCode());
			}
			
			BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"));
			
			String output;
			while ((output = br.readLine()) != null) {
				bob.append(output);
			}
			
			conn.disconnect();
		
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		} catch (KeyManagementException e) {
			e.printStackTrace();
		}
		
		JSONObject jsonObject = new JSONObject(bob.toString());
		
		return jsonObject;
		
	}
	
}
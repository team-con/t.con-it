package de.teamcon.automation;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;
import java.util.HashMap;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;

import org.apache.commons.lang3.exception.ExceptionUtils;
import org.json.JSONException;
import org.json.JSONObject;

import de.teamcon.db.DB;
import de.teamcon.db.JiraDB;
import de.teamcon.email.Email;
import de.teamcon.field.Jira;
import de.teamcon.powershell.PowerShell;


/**
 * @author feldmern
 * Access path: "/Automation/rest/server/..."
 */
@Path("server")
public class ServerService {
	
	// Ti Ta Test
	private static Logger jlog = Logger.getLogger("User Automation");
//	private static FileHandler fh;
	
	/**
	 * Writes data from webhook to data base "tcdb01.team-con.de/TconDB".
	 * User information provided by jira webhook.
	 * Comments data to ticket with the given key.
	 * 
	 * @param key						jira ticket key
	 * @param json						json sent by jira webhook
	 * @throws SecurityException 
	 * @throws IOException				
	 * @throws JSONException			
	 * @throws SQLException 
	 * @throws InterruptedException		
	 */
	@POST
	@Path("new/{key}")
	//@Consumes(MediaType.APPLICATION_JSON)
	public void postActionNewServer(@PathParam("key") final String key, final String json) throws SecurityException, IOException, JSONException, SQLException {
		
		/*fh = new FileHandler("C:\\Users\\admin.feldmern\\Documents\\Automation\\Log\\server_automation.log");  
        jlog.addHandler(fh);
        SimpleFormatter formatter = new SimpleFormatter();  
        fh.setFormatter(formatter);*/

        String netzwerkadapter = "";
        String ip = "";
        String cluster = "";
        String scsi_type = "";
        String osCustSpec = "";
        String body = "";
        String template = "";
        String hv = "";
        String domain = "";
        HashMap<String, String> osData = new HashMap<String, String>();
        int cpu = 0;
        int hdd2 = 0;
        int hdd3 = 0;
        int hdd4 = 0;
		
		JSONObject jsonobj = new JSONObject(json);

		HashMap<Jira, String> found = new HashMap<Jira, String>();
		for (Jira jira : Jira.values()) {
			try {
				
				JSONObject issue = (JSONObject) jsonobj.get("issue");
				JSONObject fields = (JSONObject) issue.get("fields");
				
				Object temp = fields.get(jira.getField());
				if (temp != null) {
					try {
						JSONObject temp2 = (JSONObject) temp;
						try {
							found.put(jira, temp2.getString("value"));
						} catch (Exception e) {
							try {
								found.put(jira, temp2.getString("name"));
							} catch (Exception e1) {
								found.put(jira, temp2.getString("displayName"));
							}
						}
					} catch (Exception e) {
						found.put(jira, temp + "");
					}
				}
			} catch (Exception e) {
				jlog.logp(Level.WARNING, "de.teamcon.automation.WebhookService", "postActionCreateServer Line 364-450", ExceptionUtils.getStackTrace(e));
			}
		}
		
		cpu = Integer.parseInt(found.get(Jira.CPU));
		
		try {
			hdd2 = Integer.parseInt(found.get(Jira.HDD2).split("[.]")[0]);
		} catch (Exception e) {
			// nix
		}
		
		try {
			hdd3 = Integer.parseInt(found.get(Jira.HDD3).split("[.]")[0]);
		} catch (Exception e) {
			// nix
		}
		
		try {
			hdd4 = Integer.parseInt(found.get(Jira.HDD4).split("[.]")[0]);
		} catch (Exception e) {
			// nix
		}
		
		String installation = found.get(Jira.INSTALLATION);
		String folder = found.get(Jira.KUNDE);
		String os = found.get(Jira.OS);
		int ram = Integer.parseInt(found.get(Jira.RAM).split("[.]")[0]);
		String servername = found.get(Jira.SERVERNAME).replace(" ", "");
		servername = servername.toUpperCase();
		String systemart = found.get(Jira.SYSTEMART);
		String zweck = found.get(Jira.ZWECK);
		hv = found.get(Jira.HV);
		int netzbereichId = getNetzbereichId(folder);

		HashMap<String, String> netzbereichDaten = getNetzbereichDaten(netzbereichId);
		
		String subnetzMaske = netzbereichDaten.get("subnetz_maske");
		String gateway = netzbereichDaten.get("gateway");
		String dns1 = netzbereichDaten.get("dns1");
		String dns2 = netzbereichDaten.get("dns2");
		String dns3 = netzbereichDaten.get("dns3");
		String dns4 = netzbereichDaten.get("dns4");
		String dns5 = netzbereichDaten.get("dns5");
		
		if (installation.indexOf("DMZ") > -1 && folder.indexOf("_S") < 0) {
			domain = "TCONRZDMZ";
			netzwerkadapter = "T-CON-DMZ-VLAN810-10.50.6.0/25";
			subnetzMaske = "255.255.255.128";
			gateway = "10.50.6.126";
			dns1 = "10.50.2.1";
			dns2 = "10.50.2.2";
			netzbereichId = 8;
		} else {
			domain = netzbereichDaten.get("standard_domain");
			netzwerkadapter = netzbereichDaten.get("netzwerkadapter");
		}
		
		if (installation.indexOf("HANA") > -1) {
			cluster = "TCESXCLRRZ04";
		} else {
			cluster = "TCESXCLRRZ03";
		}
		
		if(os.indexOf("Windows") > -1) {
			
			osCustSpec = netzbereichDaten.get("os_cust_spec");
			
			osData  = getOsData(os);
			
			scsi_type = osData.get("scsi_type");
			template = osData.get("template");
			
		} else {
			scsi_type = "ParaVirtual";
			osCustSpec = "SLES_Automation";
			template = "SLES_12_Template";
		}
		
		boolean exists = checkIfServerExists(servername);
		
		if (!exists) {
			
			if (servername.indexOf(".") < 0) {
				
				ip = getNextIp(netzbereichId, servername, domain, zweck, hv);
				String sql = "INSERT INTO nmt_serveranlage (servername, domaene, kunde, ip_adresse, netzwerkadapter, installation, betriebssystem, cpu_kerne, ram, hdd2, hdd3, hdd4, systemart, ticketkey, cluster, scsi_type, os_cust_spec, subnetz_maske, gateway, dns1, dns2, dns3, dns4, dns5, template, lizenz_key) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
				
				Connection conn = null;
				
				try {
					
					conn = DB.getConnection();
					PreparedStatement ps = conn.prepareStatement(sql);
					
					ps.setString(1, servername);
					ps.setString(2, domain);
					ps.setString(3, folder);
					ps.setString(4, ip);
					ps.setString(5, netzwerkadapter);
					ps.setString(6, installation);
					ps.setString(7, os);
					ps.setInt(8, cpu);
					ps.setInt(9, ram);
					ps.setInt(10, hdd2);
					ps.setInt(11, hdd3);
					ps.setInt(12, hdd4);
					ps.setString(13, systemart);
					ps.setString(14, key);
					ps.setString(15, cluster);
					ps.setString(16, scsi_type);
					ps.setString(17, osCustSpec);
					ps.setString(18, subnetzMaske);
					ps.setString(19, gateway);
					ps.setString(20, dns1);
					ps.setString(21, dns2);
					ps.setString(22, dns3);
					ps.setString(23, dns4);
					ps.setString(24, dns5);
					ps.setString(25, template);
					ps.setString(26, osData.get("lizenz"));
					
					int result = ps.executeUpdate();
					
					
					if (result == 1) {
						
						StringBuilder bob = new StringBuilder();
						bob.append("Daten zur Serveranlage:\n");
						bob.append("Servername: " + servername + "\n");
						bob.append("Domaene: " + domain + "\n");
						bob.append("Kunde: " + folder + "\n");
						bob.append("IP-Adresse: " + ip + "\n");
						bob.append("Netzwerkadapter: " + netzwerkadapter + "\n");
						bob.append("Installation: " + installation + "\n");
						bob.append("Betriebssystem: " + os + "\n");
						bob.append("CPU-Kerne: " + cpu + "\n");
						bob.append("Arbeitsspeicher: " + ram + "\n");
						bob.append("Festplatte 2: " + hdd2 + "\n");
						bob.append("Festplatte 3: " + hdd3 + "\n");
						bob.append("Festplatte 4: " + hdd4 + "\n");
						bob.append("Systemart: " + systemart + "\n");
						bob.append("Cluster: " + cluster + "\n");
						bob.append("SCSI-Typ: " + scsi_type + "\n");
						bob.append("Gastanpassung: " + osCustSpec + "\n");
						bob.append("Subnetzmaske: " + subnetzMaske + "\n");
						bob.append("Gateway: " + gateway + "\n");
						bob.append("DNS1: " + dns1 + "\n");
						bob.append("DNS2: " + dns2 + "\n");
						bob.append("DNS3: " + dns3 + "\n");
						bob.append("DNS4: " + dns4 + "\n");
						bob.append("DNS5: " + dns5 + "\n");
						bob.append("Template: " + template + "\n");
						
						body = bob.toString();
						
					} else {
						
						body = "<h2 style=\"z-index:auto;\"><span style=\"color:#FF0000;\"><b><span style=\"background-color: rgb(240, 240, 240);\">!!! FEHLER BEIM SCHREIBEN IN DIE DATENBANK !!!</span></b></span></h2>";
						
					}
					
				} catch(Exception e) {
					jlog.warning(ExceptionUtils.getStackTrace(e));
				} finally {
					DB.closeConnection(conn);
				}
			
			} else {
				
				body = "<h2 style=\"z-index:auto;\"><span style=\"color:#FF0000;\"><b><span style=\"background-color: rgb(240, 240, 240);\">!!! SERVERNAME DARF KEINEN PUNKT ENTHALTEN !!!</span></b></span></h2>";
				
			}
		
		} else {
			
			body = "<h2 style=\"z-index:auto;\"><span style=\"color:#FF0000;\"><b><span style=\"background-color: rgb(240, 240, 240);\">!!! SERVERNAME EXISTIERT BEREITS (Quelle: IP-NMT) !!!</span></b></span></h2>";
			
		}
		
		Email.sendMail(key, body);
		
	}
	
	/**
	 * Fetches data form data base and trigger PowerShell script to create new server.
	 * Also adds value to jira field "System" for mask "Jetzt Antrag stellen"
	 * 
	 * @param key				jira ticket key
	 * @throws IOException
	 */
	@POST
	@Path("create/{key}")
	//@Consumes(MediaType.APPLICATION_JSON)
	public void postActionCreateServer(@PathParam("key") final String key) throws IOException {
		
		Connection conn = null;
		String str = "";
		
		String servername = "";
		String domain = "";
		String folder = "";
		String ip = "";
		String netzwerkadapter = "";
		String installation = "";
		String os = "";
		int cpu = 0;
		int ram = 0;
		int hdd2 = 0;
		int hdd3 = 0;
		int hdd4 = 0;
		String systemart = "";
		String cluster = "";
		String scsiType = "";
		String osCustSpec = "";
		String subnetzMaske = "";
		String gateway = "";
		String dns1 = "";
		String dns2 = "";
		String dns3 = "";
		String dns4 = "";
		String dns5 = "";
		String template = "";
		String osKey = "";
		
		try {
			
			conn = DB.getConnection();
			Statement stmt = conn.createStatement();
			
			String sql = "SELECT * FROM nmt_serveranlage WHERE ticketkey = '" + key + "';";
			
			ResultSet rs = stmt.executeQuery(sql);
			
			if (rs.isBeforeFirst()) {
				while (rs.next()) {
					servername = rs.getString(2);
					domain = rs.getString(3);
					folder = rs.getString(4);
					ip = rs.getString(5);
					netzwerkadapter = rs.getString(6);
					installation = rs.getString(7);
					os = rs.getString(8);
					cpu = rs.getInt(9);
					ram = rs.getInt(10);
					hdd2 = rs.getInt(11);
					hdd3 = rs.getInt(12);
					hdd4 = rs.getInt(13);
					systemart = rs.getString(14);
					cluster = rs.getString(16);
					scsiType = rs.getString(17);
					osCustSpec = rs.getString(18);
					subnetzMaske = rs.getString(19);
					gateway = rs.getString(20);
					dns1 = rs.getString(21);
					dns2 = rs.getString(22);
					dns3 = rs.getString(23);
					dns4 = rs.getString(24);
					dns5 = rs.getString(25);
					template = rs.getString(26);
					osKey = rs.getString(27);
				}
				
				StringBuilder bob = new StringBuilder();
	//			bob.append("C:\\Windows\\System32\\WindowsPowerShell\\v1.0\\powershell.exe -noexit -command \"C:\\Users\\feldmern\\Documents\\DeployVM.ps1");
				bob.append("\"C:\\Windows\\System32\\WindowsPowerShell\\v1.0\\powershell.exe\" -noexit -command \"C:\\Automation\\Scripts\\VMware\\DeployVM.ps1");
				bob.append(" '"+ servername + "'");
				bob.append(" '"+ ip + "'");
				bob.append(" '"+ subnetzMaske + "'");
				bob.append(" '"+ gateway + "'");
				bob.append(" '"+ dns1 + "'");
				bob.append(" '"+ dns2 + "'");
				bob.append(" '"+ dns3 + "'");
				bob.append(" '"+ dns4 + "'");
				bob.append(" '"+ dns5 + "'");
				bob.append(" '"+ folder + "'");
				bob.append(" '"+ template + "'");
				bob.append(" '"+ cluster + "'");
				bob.append(" '"+ osCustSpec + "'");
				bob.append(" '"+ netzwerkadapter + "'");
				bob.append(" '"+ ram + "'");
				bob.append(" '"+ cpu + "'");
				bob.append(" '"+ domain + "'");
				bob.append(" '"+ scsiType + "'");
				bob.append(" '"+ hdd2 + "'");
				bob.append(" '"+ hdd3 + "'");
				bob.append(" '"+ hdd4 + "'");
				bob.append(" '"+ osKey + "'");
				bob.append(" '"+ key + "'");
				bob.append("\"");
				String command = bob.toString();
				
				try {
					str = PowerShell.executePS(Runtime.getRuntime(), str, command);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				
//				boolean success = Email.sendMail(key, str);
//				
//				if (!success) {
//					jlog.info("Message could not be sent!");
//				}
				
				int parentOptionId = getParentId(folder);
				
				if (parentOptionId == 0) {
					
					addOptionToJiraField(folder, 0);
					Thread.sleep(2000); // 420 Lodere Es!
					parentOptionId = getParentId(folder);
					
				}
				
				addOptionToJiraField(servername, parentOptionId);

				String fqdn = servername + "." + domain;
				newAsgBaseItem(folder, servername, ip, fqdn);
			}
			
		} catch (Exception ex) {
			jlog.warning(ExceptionUtils.getStackTrace(ex));
		} finally {
			DB.closeConnection(conn);
		}

	}
	
	/**
	 * Clears ip entry and deletes entry from table "serveranlage" if ticket was deleted.
	 * 
	 * @param key		jira ticket key
	 */
	@POST
	@Path("cancel/{key}")
	//@Consumes(MediaType.APPLICATION_JSON)
	public void postActionCancel(@PathParam("key") final String key) {
		
		boolean exists = checkIfTicketExists(key);
		
		if (exists) {
		
			String ip = getIpFromServer(key);
			clearIpEntry(ip);
			
			String sql = "DELETE FROM nmt_serveranlage WHERE ticketkey LIKE '" + key + "'";
			Connection conn = null;
			
			try {
				
				conn = DB.getConnection();
				Statement stmt = conn.createStatement();
				
				stmt.executeUpdate(sql);
				
			} catch(Exception e) {
				jlog.warning(ExceptionUtils.getStackTrace(e));
			} finally {
				DB.closeConnection(conn);
			}
			
		}
		
	}
	
	/**
	 * Executes Post Action for path ".../delete/<ticket_key>"
	 * 
	 * @param key				ticket key
	 * @param json				json with ticket data
	 * @throws JSONException
	 * @throws SQLException
	 */
	@POST
	@Path("delete/{key}")
	public void postActionDelete(@PathParam("key") final String key, final String json) throws JSONException, SQLException {
		
		JSONObject jsonobj = new JSONObject(json);

		HashMap<Jira, String> found = new HashMap<Jira, String>();
		for (Jira jira : Jira.values()) {
			try {
				
				JSONObject issue = (JSONObject) jsonobj.get("issue");
				JSONObject fields = (JSONObject) issue.get("fields");
				
				Object temp = fields.get(jira.getField());
				if (temp != null) {
					try {
						JSONObject temp2 = (JSONObject) temp;
						try {
							try {
								JSONObject temp3 = (JSONObject) temp2.get("child");
								found.put(jira, temp2.getString("value") + "&" + temp3.getString("value"));
							} catch (Exception e) {
								found.put(jira, temp2.getString("value"));
							}
						} catch (Exception e) {
							try {
								found.put(jira, temp2.getString("name"));
							} catch (Exception e1) {
								found.put(jira, temp2.getString("displayName"));
							}
						}
					} catch (Exception e) {
						found.put(jira, temp + "");
					}
				}
			} catch (Exception e) {
				jlog.logp(Level.WARNING, "de.teamcon.automation.WebhookService", "postActionCreateServer Line 364-450", ExceptionUtils.getStackTrace(e));
			}
		}
		
		String system = found.get(Jira.SYSTEM);
		String folder = system.split("&")[0];
		String servername = system.split("&")[1];
		
		int netzbereichId = getNetzbereichId(folder);
		String ip = getIpFromServer(netzbereichId, servername);
		
		Connection conn = null;
		String sql = "INSERT INTO nmt_serverloeschung (kunde, servername, ip_adresse, ticketkey) VALUES (?,?,?,?)";
		
		try {
			
			conn = DB.getConnection();
			PreparedStatement ps = conn.prepareStatement(sql);
			
			ps.setString(1, folder);
			ps.setString(2, servername);
			ps.setString(3, ip);
			ps.setString(4, key);
			
			ps.executeUpdate();
			
		} catch (Exception e) {
			jlog.warning(ExceptionUtils.getStackTrace(e));
		} finally {
			DB.closeConnection(conn);
		}
		
	}
	
	/**
	 * Executes Post Action for path "../execDelete/<ticket_key>"
	 * 
	 * @param key		ticket key
	 */
	@POST
	@Path("execDelete/{key}")
	public void postActionExecDelete(@PathParam("key") final String key) {
		
		String folder = "";
		String servername = "";
		String ip = "";
		String str = "";
		
		String sql = "SELECT * FROM nmt_serverloeschung WHERE ticketkey LIKE '" + key + "'";
		Connection conn = null;
		
		try {
			
			conn = DB.getConnection();
			Statement stmt = conn.createStatement();
			
			ResultSet rs = stmt.executeQuery(sql);
			
			while(rs.next()) {
				folder = rs.getString("kunde");
				servername = rs.getString("servername");
				ip = rs.getString("ip_adresse");
			}
			
			StringBuilder bob = new StringBuilder();
			bob.append("\"C:\\Windows\\System32\\WindowsPowerShell\\v1.0\\powershell.exe\" -noexit -command \"C:\\Automation\\Scripts\\VMware\\DeleteVM.ps1");
			bob.append(" '"+ folder + "'");
			bob.append(" '"+ servername + "'");
			bob.append(" '"+ ip + "'");
			bob.append(" '"+ key + "'");
			bob.append("\"");
			
			String command = bob.toString();
			
			try {
				str = PowerShell.executePS(Runtime.getRuntime(), str, command);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			
		} catch (Exception e) {
			jlog.warning(ExceptionUtils.getStackTrace(e));
		} finally {
			DB.closeConnection(conn);
		}
		
	}
	
	/**
	 * Returns ip address of existing server
	 * 
	 * @param netzbereichId		id of network
	 * @param servername		name of server to be created
	 * @return ip				IP-Address of server
	 */
	private String getIpFromServer(int netzbereichId, String servername) {
		String ip = "";
		String sql = "SELECT ip_adresse from nmt_ip_eintrag WHERE netzbereich_id = " + netzbereichId + " AND name LIKE '" + servername + "'";
		Connection conn = null;
		
		try {
			
			conn = DB.getConnection();
			Statement stmt = conn.createStatement();
			
			ResultSet rs = stmt.executeQuery(sql);
			
			while(rs.next()) {
				ip = rs.getString("ip_adresse");
			}
			
		} catch (Exception e) {
			jlog.warning(ExceptionUtils.getStackTrace(e));
		} finally {
			DB.closeConnection(conn);
		}
		
		return ip;
	}
	
	/**
	 * Returns next free ip address in given network.
	 * Updates data base entry with server name, purpose and responsible person.
	 * 
	 * @param netzbereichId		id of network
	 * @param servername		name of server to be created
	 * @param domain			domain of server to be created
	 * @param zweck				purpose of server to be created
	 * @param hv				responsible person for server to be created
	 * @return ip				next free ip address in network
	 * @throws SQLException
	 */
	private String getNextIp (int netzbereichId, String servername, String domain, String zweck, String hv) throws SQLException {
		
		String ip = "";
		int id = 0;
		Connection conn = null;
		
		try {
			conn = DB.getConnection();
			Statement stmt = conn.createStatement();
			
			String sql = "SELECT TOP 1 id, ip_adresse FROM nmt_ip_eintrag WHERE name = '' AND netzbereich_id = " + netzbereichId + " ORDER BY id, ip_adresse ASC;";
			
			ResultSet rs = stmt.executeQuery(sql);
			
			while (rs.next()) {
				ip = rs.getString("ip_adresse");
				id = rs.getInt("id");
			}
			
			stmt.close();
			
			String update = "UPDATE nmt_ip_eintrag SET netzbereich_id = " + netzbereichId + ", name = '" + servername + "', domaene = '" + domain + "', zweck = '" + zweck + "', hauptverantwortlicher = '" + hv + "' WHERE id = " + id + ";";
			
			Statement stmt2 = conn.createStatement();
			
			stmt2.executeUpdate(update);
			
			stmt2.close();
			
		} catch(Exception e) {
			jlog.warning(ExceptionUtils.getStackTrace(e));
		} finally {
			DB.closeConnection(conn);
		}
		
		return ip;
		
	}
	
	/**
	 * Fetches data of given network.
	 * 
	 * @param netzbereichId		id of network
	 * @return map				HashMap with fetched data
	 */
	private HashMap<String, String> getNetzbereichDaten(int netzbereichId) {
		
		HashMap<String, String> map = new HashMap<String, String>();
		Connection conn = null;
		
		try {
			
			conn = DB.getConnection();
			Statement stmt = conn.createStatement();
			
			String sql = "SELECT subnetz_maske, gateway, dns1, dns2, dns3, dns4, dns5, netzwerkadapter, standard_domain, os_cust_spec FROM nmt_netzbereich WHERE id = " + netzbereichId;
			
			ResultSet rs = stmt.executeQuery(sql);
			
			while (rs.next()) {
				
				map.put("subnetz_maske", rs.getString(1));
				map.put("gateway", rs.getString(2));
				
				if (!rs.getString(3).equals("")) {
					map.put("dns1", rs.getString(3));
				} else {
					map.put("dns1", "null");
				}
				
				if (!rs.getString(4).equals("")) {
					map.put("dns2", rs.getString(4));
				} else {
					map.put("dns2", "null");
				}
				
				if (!rs.getString(5).equals("")) {
					map.put("dns3", rs.getString(5));
				} else {
					map.put("dns3", "null");
				}
				
				if (!rs.getString(6).equals("")) {
					map.put("dns4", rs.getString(6));
				} else {
					map.put("dns4", "null");
				}
				
				if (!rs.getString(7).equals("")) {
					map.put("dns5", rs.getString(7));
				} else {
					map.put("dns5", "null");
				}

				map.put("netzwerkadapter", rs.getString(8));
				map.put("standard_domain", rs.getString(9));
				map.put("os_cust_spec", rs.getString(10));
				
			}
			
		} catch (Exception e) {
			jlog.warning(ExceptionUtils.getStackTrace(e));
		} finally {
			DB.closeConnection(conn);
		}
		
		return map;
	}
	
	/**
	 * Returns network id of given folder.
	 * 
	 * @param folder			name of folder
	 * @return netzbereichId	id of network
	 * @throws SQLException
	 */
	private int getNetzbereichId(String folder) throws SQLException {
		int netzbereichId = 0;
		Connection conn = null;
		
		try {
			
			conn = DB.getConnection();
			Statement stmt = conn.createStatement();
			
			String sql = "SELECT n.id FROM nmt_netzbereich n WHERE n.kunde LIKE '" + folder + "'";
			
			ResultSet rs = stmt.executeQuery(sql);
			
			while (rs.next()) {
				netzbereichId = rs.getInt("id");
			}
			
		} catch (Exception e) {
			jlog.warning(ExceptionUtils.getStackTrace(e));
		} finally {
			DB.closeConnection(conn);
		}
		
		return netzbereichId;
	}
	
	/**
	 * Adds value to jira field "System"
	 * 
	 * @param customvalue			value to be added
	 * @param parentOptionId		id of parent option (usually id of parent value in first drop down select)
	 */
	private void addOptionToJiraField (String customvalue, int parentOptionId) {
		
		Connection conn = null;
		String sql = "INSERT INTO jiraschema.customfieldoption (ID,CUSTOMFIELD,CUSTOMFIELDCONFIG,PARENTOPTIONID,SEQUENCE,customvalue,optiontype,disabled) VALUES (?,?,?,?,?,?,?)";
		int seq = getNextSequence(parentOptionId);
		
		try {

			conn = JiraDB.getConnection();
			PreparedStatement ps = conn.prepareStatement(sql);
			
			ps.setInt(1, getNextId());
			ps.setInt(2, 11611);;
			ps.setInt(3, 12232);
			
			if (parentOptionId == 0) {
				
				ps.setNull(4, Types.DOUBLE);
				
			} else {
				
				ps.setInt(4, parentOptionId);
				
			}
			
			ps.setInt(5, seq);
			ps.setString(6, customvalue);
			ps.setNull(7, Types.VARCHAR);
			ps.setString(8, "N");
			
		} catch(Exception e) {
			jlog.warning(ExceptionUtils.getStackTrace(e));
		} finally {
			JiraDB.closeConnection(conn);
		}
		
	}
	
	/**
	 * Returns next id in table "jiraschema.customfieldoption"
	 * 
	 * @return id				next id in table "jiraschema.customfieldoption"
	 */
	private int getNextId() {
		
		String sql;
		
		sql = "SELECT MAX(ID) FROM jiraschema.customfieldoption";
		
		Connection conn = null;
		int id = 0;
		
		try {
			
			conn = JiraDB.getConnection();
			Statement stmt = conn.createStatement();
			
			ResultSet rs = stmt.executeQuery(sql);
			rs.next();
			id = rs.getInt(1) + 1;
			
		} catch(Exception e) {
			jlog.warning(ExceptionUtils.getStackTrace(e));
		} finally {
			JiraDB.closeConnection(conn);
		}
		
		return id;
		
	}
	
	/**
	 * Returns next sequence number for value to be added with given parent id
	 * 
	 * @param parentOptionId			id of parent option (usually id of parent value in first drop down select)
	 * @return seq						next sequence number for value to be added with given parent id
	 */
	private int getNextSequence(int parentOptionId) {
		
		String sql;
		
		if (parentOptionId == 0) {
			
			sql = "SELECT MAX(SEQUENCE) FROM jiraschema.customfieldoption WHERE CUSTOMFIELD = 11611 AND PARENTOPTIONID IS NULL";
			
		} else {
		
			sql = "SELECT MAX(SEQUENCE) FROM jiraschema.customfieldoption WHERE CUSTOMFIELD = 11611 AND PARENTOPTIONID = " + parentOptionId;
			
		}
		
		Connection conn = null;
		int seq = 0;
		
		try {
			
			conn = JiraDB.getConnection();
			Statement stmt = conn.createStatement();
			
			ResultSet rs = stmt.executeQuery(sql);
			rs.next();
			seq = rs.getInt(1) + 1;
			
		} catch(Exception e) {
			jlog.warning(ExceptionUtils.getStackTrace(e));
		} finally {
			JiraDB.closeConnection(conn);
		}
		
		return seq;
	}
	
	/**
	 * Returns parent id with given folder name
	 * 
	 * @param folder			name of folder
	 * @return parentId			parent id
	 */
	private int getParentId (String folder) {
		
  		String sql = "SELECT ID FROM jiraschema.customfieldoption WHERE CUSTOMFIELD = 11611 AND customvalue LIKE '" + folder + "'";
		Connection conn = null;
		int parentId = 0;
		
		try {
			
			conn = JiraDB.getConnection();
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);
			
			if (rs.isBeforeFirst() ) {    
			    while(rs.next()) {
			    	parentId = rs.getInt("ID");
			    }
			} 
			
			
		} catch(Exception e) {
			jlog.warning(ExceptionUtils.getStackTrace(e));
		} finally {
			JiraDB.closeConnection(conn);
		}
		
		return parentId;
		
	}
	
	/**
	 * Checks if data base entry with ticket name exists
	 * 
	 * @param ticketkey			ticket key to search for
	 * @return exists			true if data base entry exitst, false if not
	 */
	private boolean checkIfTicketExists(String ticketkey) {
		
		boolean exists = false;
		
		String sql = "SELECT * FROM nmt_serveranlage WHERE ticketkey LIKE '" + ticketkey + "'";
		Connection conn = null;
		
		try {
			
			conn = DB.getConnection();
			Statement stmt = conn.createStatement();
			
			ResultSet rs = stmt.executeQuery(sql);
			
			if (rs.isBeforeFirst()) {
				exists = true;
			}
			
		} catch (Exception e) {
			jlog.warning(ExceptionUtils.getStackTrace(e));
		} finally {
			DB.closeConnection(conn);
		}
		
		return exists;
		
	}
	
	/**
	 * Returns license key for given OS
	 * 
	 * @param os		name of OS
	 * @return key		license key for OS
	 */
	private HashMap<String, String> getOsData(String os) {
		
		HashMap<String, String> result = new HashMap<String, String>();
		
		String sql = "SELECT * FROM nmt_os_lizenz WHERE os LIKE '" + os + "'";
		Connection conn = null;
		
		try {
		
			conn = DB.getConnection();
			Statement stmt = conn.createStatement();
			
			ResultSet rs = stmt.executeQuery(sql);
			
			while(rs.next()) {
				
				result.put("lizenz", rs.getString(3));
				result.put("scsi_type", rs.getString(4));
				result.put("template", rs.getString(5));
				
			}
			
		} catch (Exception e) {
			jlog.warning(ExceptionUtils.getStackTrace(e));
		} finally {
			DB.closeConnection(conn);
		}
		
		return result;
	}
	
	/**
	 * Returns ip from entry in table "serveranlage" depending on ticket key.
	 * 
	 * @param ticketkey		jira ticket key
	 * @return ip 			ip adress of entry
	 */
	private String getIpFromServer(String ticketkey) {
		
		String ip = "";
		String sql = "SELECT ip_adresse FROM nmt_serveranlage WHERE ticketkey LIKE '" + ticketkey + "'";
		Connection conn = null;
		
		try {
			
			conn = DB.getConnection();
			Statement stmt = conn.createStatement();
			
			ResultSet rs = stmt.executeQuery(sql);
			
			while(rs.next()) {
				ip = rs.getString("ip_adresse");
			}
			
		} catch (Exception e) {
			jlog.warning(ExceptionUtils.getStackTrace(e));
		} finally {
			DB.closeConnection(conn);
		}
		
		return ip;
		
	}
	
	/**
	 * Clears entry in table "ip_eintrag" depending on ip.
	 * 
	 * @param ip				ip of entry to be cleared
	 * @return success 			true if update was successful, false if not
	 */
	private boolean clearIpEntry(String ip) {
		
		boolean success = false;
		
		String sql = "UPDATE nmt_ip_eintrag SET name = '', zweck = '', hauptverantwortlicher = '', externe_ip_dns = '' WHERE ip_adresse LIKE '" + ip + "'";
		Connection conn = null;
		
		try {
			
			conn = DB.getConnection();
			Statement stmt = conn.createStatement();
			
			int result = stmt.executeUpdate(sql);
			
			if(result == 1) {
				success = true;
			}
			
		} catch (Exception e) {
			jlog.warning(ExceptionUtils.getStackTrace(e));
		} finally {
			DB.closeConnection(conn);
		}
		
		return success;
	}
	
	/**
	 * Checks if entry in table "ip_eintrag" with given servername exists.
	 * 
	 * @param servername		name of server
	 * @return exists 			true if servername exists, false if not
	 */
	private boolean checkIfServerExists(String servername) {
		
		String searchString = servername.toUpperCase();
		boolean exists = false;
		
		String sql = "SELECT * FROM nmt_ip_eintrag WHERE name LIKE '" + searchString + "'";
		Connection conn = null;
		
		try {
			
			conn = DB.getConnection();
			Statement stmt = conn.createStatement();
			
			ResultSet rs = stmt.executeQuery(sql);
			
			if (rs.isBeforeFirst()) {
				exists = true;
			}
			
		} catch (Exception e) {
			jlog.warning(ExceptionUtils.getStackTrace(e));
		} finally {
			DB.closeConnection(conn);
		}
		
		return exists;
		
	}
	
	/**
	 * Creates new ASGRD Base Item for created Server
	 * 
	 * @param folder			Where to put the item
	 * @param servername		Item name
	 * @param ip				Ip of server -> is used to get purpose and FQDN of server
	 * @param fqdn				Fully Qualified Domain Name of Item
	 */
	private void newAsgBaseItem(String folder, String servername, String ip, String fqdn) {
		
		String str = "";
		String zweck = getZweck(ip);
		
		StringBuilder bob = new StringBuilder();
		bob.append("\"C:\\Windows\\syswow64\\WindowsPowerShell\\v1.0\\powershell.exe\" -noexit -command \"C:\\Automation\\Scripts\\VMware\\asgNewBaseItem.ps1");
		bob.append(" '"+ folder + "'");
		bob.append(" '"+ servername + "'");
		bob.append(" '"+ zweck + "'");
		bob.append(" '"+ fqdn + "'");
		bob.append("\"");
		
		String command = bob.toString();
		
		try {
			str = PowerShell.executePS(Runtime.getRuntime(), str, command);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Returns full qualified domain name, e.g. 'APPGAUT01.tcon-group.de'
	 * 
	 * @param ip		Ip of server
	 * @return fqdn		full qualified domain name
	 */
	private String getFQDN(String ip) {
		
		String name = "";
		String domain = "";
		String fqdn = "";
		
		String sql = "SELECT name, domaene FROM nmt_ip_eintrag WHERE ip_adresse LIKE '" + ip + "'";
		Connection conn = null;
		
		try {
			
			conn = DB.getConnection();
			Statement stmt = conn.createStatement();
			
			ResultSet rs = stmt.executeQuery(sql);
			
			while(rs.next()) {
				name = rs.getString("name");
				domain = rs.getString("domaene");
			}
			
			
		} catch (Exception e) {
			jlog.warning(ExceptionUtils.getStackTrace(e));
		} finally {
			DB.closeConnection(conn);
		}
		
		if (domain != null && domain != "") {
			fqdn = name + "." + domain;
		} else {
			fqdn = name;
		}
		
		return fqdn;
	}
	
	/**
	 * Returns Purpose of created server 
	 * 
	 * @param ip		Ip of server
	 * @return zweck	Purpose of server
	 */
	private String getZweck(String ip) {
		String zweck = "";
		
		String sql = "SELECT zweck FROM nmt_ip_eintrag WHERE ip_adresse LIKE '" + ip + "'";
		Connection conn = null;
		
		try {
			
			conn = DB.getConnection();
			Statement stmt = conn.createStatement();
			
			ResultSet rs = stmt.executeQuery(sql);
			
			while(rs.next()) {
				zweck = rs.getString("zweck");
			}
		} catch (Exception e) {
			jlog.warning(ExceptionUtils.getStackTrace(e));
		} finally {
			DB.closeConnection(conn);
		}
		
		return zweck;
	}
	
}
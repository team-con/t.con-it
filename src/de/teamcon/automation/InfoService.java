package de.teamcon.automation;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.google.gson.Gson;

/**
 * @author feldmern
 */
@Path("info")
public class InfoService {

	/**
	 * Info method (No-op)
	 * 
	 * @return "Success!" if service can be contacted
	 */
	@GET
	@Path("ping")
	@Produces(MediaType.APPLICATION_JSON)
	public String info() {
		return new Gson().toJson("Success!");
	}
}

package de.teamcon.powershell;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.commons.lang3.exception.ExceptionUtils;

/**
 * @author feldmern
 */
public class PowerShell {

	private static Logger jlog = Logger.getLogger("User Automation");
	private static FileHandler fh;
	
	/**
	 * Executes a given command and return output as string
	 * 
	 * @param runtime					Application environment 
	 * @param str						Empty string; Will be filled with output from PowerShell script
	 * @param command					Command to be executed
	 * @return str						Output string with information from executing PowerShell script
	 * @throws InterruptedException		if Thread.sleep() fails
	 */
	public static String executePS(Runtime runtime, String str, String command) throws InterruptedException {
		
		String line;
		Process process = null;

		try {
			process = runtime.exec(command);
			process.getOutputStream().close();
		} catch (Exception e) {
			jlog.logp(Level.SEVERE, "de.teamcon.automation.WebhookService", "executePS process", ExceptionUtils.getStackTrace(e));
		}
		
		str += " Standard Output:";
		
		try {
			BufferedReader stdout = new BufferedReader(new InputStreamReader(process.getInputStream()));
			Thread.sleep(500);
			
			while (stdout.ready()) {
				line = stdout.readLine();
				if (line != null) {
					str += " " + line;
				}
			}
			
			stdout.close();
		} catch (Exception e) {
			jlog.logp(Level.SEVERE, "de.teamcon.automation.WebhookService", "executePS stdout", ExceptionUtils.getStackTrace(e));
		}

		str += " Standard Error:";
		
		try {
			BufferedReader stderr = new BufferedReader(new InputStreamReader(process.getErrorStream()));
			Thread.sleep(500);
			
			while (stderr.ready()) {
				line = stderr.readLine();
				if (line != null) {
					str += " " + line;
				}
			}
			
			stderr.close();
		} catch (Exception e) {
			jlog.logp(Level.SEVERE, "de.teamcon.automation.WebhookService", "executePS stderr", ExceptionUtils.getStackTrace(e));
		}
		
		return str;
		
	}
}
